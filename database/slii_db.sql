/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : slii_db

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-03-26 16:40:34
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `slii_activities`
-- ----------------------------
DROP TABLE IF EXISTS `slii_activities`;
CREATE TABLE `slii_activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed` varchar(1000) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_activities
-- ----------------------------
INSERT INTO `slii_activities` VALUES ('14', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident', 'Exams', 'fa fa-trophy');
INSERT INTO `slii_activities` VALUES ('15', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident', 'COURSES', 'fa fa-picture-o');
INSERT INTO `slii_activities` VALUES ('19', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident', 'Pro.Training', 'fa fa-desktop');
INSERT INTO `slii_activities` VALUES ('20', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident', 'Development', 'fa fa-globe');
INSERT INTO `slii_activities` VALUES ('22', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident	', 'Reacerch & Devs', 'fa fa-globe');

-- ----------------------------
-- Table structure for `slii_company`
-- ----------------------------
DROP TABLE IF EXISTS `slii_company`;
CREATE TABLE `slii_company` (
  `cmp_name` varchar(50) DEFAULT NULL,
  `cmp_type` int(11) DEFAULT '0',
  `cmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `cmp_description` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`cmp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_company
-- ----------------------------
INSERT INTO `slii_company` VALUES ('AIA', '1', '1', 'AIA INSURANCE', null, null);
INSERT INTO `slii_company` VALUES ('ALLIANZ', '1', '2', 'ALLIANZ INSURANCE', null, null);
INSERT INTO `slii_company` VALUES ('CEYLINCO', '0', '3', 'CEYLINCO INSURANCE', null, null);

-- ----------------------------
-- Table structure for `slii_contact_informations`
-- ----------------------------
DROP TABLE IF EXISTS `slii_contact_informations`;
CREATE TABLE `slii_contact_informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) DEFAULT NULL,
  `email` varchar(15) DEFAULT NULL,
  `subject` varchar(15) DEFAULT NULL,
  `message` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_contact_informations
-- ----------------------------
INSERT INTO `slii_contact_informations` VALUES ('1', 'asas', 'asas@gmail.com', 'asasa', 'as');

-- ----------------------------
-- Table structure for `slii_course_payment`
-- ----------------------------
DROP TABLE IF EXISTS `slii_course_payment`;
CREATE TABLE `slii_course_payment` (
  `cp_id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_mcp_id` int(11) NOT NULL,
  `amount` float(15,0) NOT NULL,
  PRIMARY KEY (`cp_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_course_payment
-- ----------------------------
INSERT INTO `slii_course_payment` VALUES ('1', '1', '2000');
INSERT INTO `slii_course_payment` VALUES ('2', '1', '2500');

-- ----------------------------
-- Table structure for `slii_course_subject_pivot`
-- ----------------------------
DROP TABLE IF EXISTS `slii_course_subject_pivot`;
CREATE TABLE `slii_course_subject_pivot` (
  `fk_course_pivot_id` int(11) DEFAULT NULL,
  `fk_subject_pivot_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `is_compulsory` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_course_subject_pivot
-- ----------------------------
INSERT INTO `slii_course_subject_pivot` VALUES ('1', '1', '1', '1');
INSERT INTO `slii_course_subject_pivot` VALUES ('1', '2', '2', '0');
INSERT INTO `slii_course_subject_pivot` VALUES ('1', '3', '3', '1');
INSERT INTO `slii_course_subject_pivot` VALUES ('2', '1', '4', '0');
INSERT INTO `slii_course_subject_pivot` VALUES ('2', '3', '5', '1');

-- ----------------------------
-- Table structure for `slii_courses`
-- ----------------------------
DROP TABLE IF EXISTS `slii_courses`;
CREATE TABLE `slii_courses` (
  `course_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(50) DEFAULT NULL,
  `course_description` varchar(100) DEFAULT NULL,
  `subjects` varchar(150) DEFAULT NULL,
  `compulsory_subjects` varchar(150) DEFAULT NULL,
  `course_fee` float(15,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_courses
-- ----------------------------
INSERT INTO `slii_courses` VALUES (null, '1', 'Diploma In Insurance Practice', 'Diploma In Insurance Practice', '1,2,3', '2,1', '10000');
INSERT INTO `slii_courses` VALUES (null, '2', 'SLII Certificate Course', 'SLII Certificate Course', '3,4,5', '3,4,5', '10000');
INSERT INTO `slii_courses` VALUES (null, '3', 'werwew', 'rewrewrw', '5', '5', '10000');
INSERT INTO `slii_courses` VALUES (null, '4', 'werwew', 'rewrewrw', '2,5,7', '5', '10000');
INSERT INTO `slii_courses` VALUES (null, '5', 'werwew', 'rewrewrw', '1,2,3,5,6,7', '6', '10000');
INSERT INTO `slii_courses` VALUES (null, '6', 'werwew', 'rewrewrw', '2,5,6,7', null, '10000');
INSERT INTO `slii_courses` VALUES (null, '7', 'werwew', 'rewrewrw', '2,3,5', null, '10000');
INSERT INTO `slii_courses` VALUES (null, '8', 'werwew', 'rewrewrw', '2,3', null, '10000');
INSERT INTO `slii_courses` VALUES (null, '9', 'course1', 'course1', '2,3,4,5,6,7,8,10', '10', '10000');

-- ----------------------------
-- Table structure for `slii_gallery_images`
-- ----------------------------
DROP TABLE IF EXISTS `slii_gallery_images`;
CREATE TABLE `slii_gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded_on` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slii_gallery_images
-- ----------------------------
INSERT INTO `slii_gallery_images` VALUES ('30', '1.jpg', 'werewrwrwerw', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('31', '2.jpg', 'eeeeeeeeee', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('32', '3.jpg', 'eeeeeeeee', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('33', '4.jpg', 'wererwe', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('49', 'Clearbtn_abuse.png', 'wew', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('50', 'Clearbtn_abuse.png', '', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('51', 'Clearbtn_abuse.png', '', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('52', 'DivisionList.png', '', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_gallery_images` VALUES ('53', 'Clearbtn_newInquiry.png', '', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for `slii_groups`
-- ----------------------------
DROP TABLE IF EXISTS `slii_groups`;
CREATE TABLE `slii_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `groupname_UNIQUE` (`groupname`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slii_groups
-- ----------------------------
INSERT INTO `slii_groups` VALUES ('1', 'INSURANCE_COMPANY', 'Insurance Company');
INSERT INTO `slii_groups` VALUES ('2', 'SLII_ADMIN', 'Slii Admin');
INSERT INTO `slii_groups` VALUES ('5', 'SLII_MEMBER', 'Slii Member');

-- ----------------------------
-- Table structure for `slii_member`
-- ----------------------------
DROP TABLE IF EXISTS `slii_member`;
CREATE TABLE `slii_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(10) DEFAULT NULL,
  `fullname` varchar(100) NOT NULL,
  `dob` varchar(10) NOT NULL,
  `nic` varchar(12) NOT NULL,
  `pref_mail_adr` int(11) NOT NULL,
  `cmp_name_adr` varchar(100) NOT NULL,
  `cmp_phone` varchar(100) NOT NULL,
  `cmp_email` varchar(20) NOT NULL,
  `home_adr` varchar(50) NOT NULL,
  `home_phone` varchar(15) NOT NULL,
  `home_email` varchar(15) NOT NULL,
  `home_mobile` varchar(15) NOT NULL,
  `present_occupation` varchar(100) NOT NULL,
  `current_industry_nature` int(11) NOT NULL,
  `insurance_type` int(11) NOT NULL,
  `edu_qualifications` varchar(100) NOT NULL,
  `edu_qualifications_other` varchar(100) NOT NULL,
  `membership_category_applied` int(11) NOT NULL,
  `ins_areas` varchar(100) NOT NULL,
  `fk_slii_user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_member
-- ----------------------------
INSERT INTO `slii_member` VALUES ('1', 'mrs', '123', '03/19/2018', '1212', '2', 'dsf', '(333) 333-3333', '33333333333', 'dsf', '(232) 323-____', '3333333', '(333) 333-3333', '3333333', '1', '1', '6', '', '0', '4', '37', null, null);
INSERT INTO `slii_member` VALUES ('2', 'ms', '1234', '03/15/2018', '12121', '2', '2', '(111) 111-1111', '1212', '121', '(221) 121-2121', '1121', '(212) 222-2222', '21212121121', '1', '1', '5', '', '0', '3', '38', null, null);
INSERT INTO `slii_member` VALUES ('3', 'mr', 'sdfsd', '03/28/2018', 'sdfsdfsdf', '2', '1111111111111', '(111) 111-1111', '1111111111111', '111', '(___) ___-__11', '1111111111', '(111) 111-1___', '111111111', '2', '1', '3', '', '0', '5', '39', null, null);
INSERT INTO `slii_member` VALUES ('4', 'ms', '111111111', '03/28/2018', '111', '1', '1111111111111111', '(111) 111-111_', '11', '1111', '(11_) ___-____', '11', '(111) 111-1111', '11', '1', '1', '1', '', '0', '1', '40', null, null);
INSERT INTO `slii_member` VALUES ('5', 'mr', '454', '03/21/2018', '4545', '2', '3434', '(434) ___-____', '343', '454', '(343) 43_-____', '434', '(343) 43_-____', '34343343', '1', '1', '4,5', '', '0', '3', '41', null, null);
INSERT INTO `slii_member` VALUES ('6', 'mr', 'udeep', '03/14/2018', 'erererere', '2', 'ere', '(232) ___-____', 'ddasd@gmail.com', '323', '(232) ___-____', 'ddasd@gmail.com', '(232) 3__-____', '2323232323', '2', '1', '2', '', '0', '2', '42', null, null);

-- ----------------------------
-- Table structure for `slii_member_course_pivot`
-- ----------------------------
DROP TABLE IF EXISTS `slii_member_course_pivot`;
CREATE TABLE `slii_member_course_pivot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_member_id` int(11) DEFAULT NULL,
  `fk_course_id` int(11) DEFAULT NULL,
  `assign_subjects` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_member_course_pivot
-- ----------------------------
INSERT INTO `slii_member_course_pivot` VALUES ('1', '38', '1', '1,2,3');
INSERT INTO `slii_member_course_pivot` VALUES ('2', '7', '1', '1,2,3');
INSERT INTO `slii_member_course_pivot` VALUES ('3', '7', '2', '3,4');
INSERT INTO `slii_member_course_pivot` VALUES ('4', '7', '9', '10,2,4,5');
INSERT INTO `slii_member_course_pivot` VALUES ('5', '7', '4', '5,2');
INSERT INTO `slii_member_course_pivot` VALUES ('6', '7', '2', '1,2,3,4,5');
INSERT INTO `slii_member_course_pivot` VALUES ('7', '7', '2', '3,4,5');

-- ----------------------------
-- Table structure for `slii_member_insurance_experience`
-- ----------------------------
DROP TABLE IF EXISTS `slii_member_insurance_experience`;
CREATE TABLE `slii_member_insurance_experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) DEFAULT NULL,
  `name_of_company` varchar(50) DEFAULT NULL,
  `job_title` varchar(15) DEFAULT NULL,
  `exp_from` varchar(15) DEFAULT NULL,
  `exp_to` varchar(15) DEFAULT NULL,
  `description_of_job` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_member_insurance_experience
-- ----------------------------
INSERT INTO `slii_member_insurance_experience` VALUES ('1', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('2', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('3', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('4', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('5', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('6', null, 'wer', 'ewe', 're', 'rwe', 'erer', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('15', '1', '22222', '222222222', '2', '222', '22', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('14', '1', 'asdas', 'sd', 'asdasd', 'sadsad', 'asda', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('13', '8', '121', '212', '121', '21212', '21', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('9', '7', 'www', 'w', 'wwwwwwwww', 'wwwwwwwwwww', 'www', '0000-00-00 00:00:00', '0');
INSERT INTO `slii_member_insurance_experience` VALUES ('12', '7', 'as', 'as', 'as', 'asasa', 'as', '0000-00-00 00:00:00', '0');
INSERT INTO `slii_member_insurance_experience` VALUES ('16', '2', '121', '', '', '', '', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('17', '3', '1', '1111', '111111111', '11111111', '1', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('18', '4', '1111111', '11111111111', '11', '11111', '1', '0000-00-00 00:00:00', '0');
INSERT INTO `slii_member_insurance_experience` VALUES ('19', '5', '34', '', '', '', '34343434', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('20', '5', '34', '', '', '', '34343434', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('21', '5', '34', '', '', '', '34343434', null, null);
INSERT INTO `slii_member_insurance_experience` VALUES ('22', '6', '23', '3', '2', '3232', '2', null, null);

-- ----------------------------
-- Table structure for `slii_news_feeds`
-- ----------------------------
DROP TABLE IF EXISTS `slii_news_feeds`;
CREATE TABLE `slii_news_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed` varchar(1000) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_news_feeds
-- ----------------------------
INSERT INTO `slii_news_feeds` VALUES ('23', 'wer', 'ewrwe');
INSERT INTO `slii_news_feeds` VALUES ('24', 'ww', 'www');
INSERT INTO `slii_news_feeds` VALUES ('2', 'Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.', '12');
INSERT INTO `slii_news_feeds` VALUES ('3', 'Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.', 'as');
INSERT INTO `slii_news_feeds` VALUES ('4', 'Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.', 'sdfdsfdfsd');
INSERT INTO `slii_news_feeds` VALUES ('6', 'Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi.', 'test2');
INSERT INTO `slii_news_feeds` VALUES ('14', 'werewr', 'werewrew');
INSERT INTO `slii_news_feeds` VALUES ('15', 'werewr', 'werewrew');
INSERT INTO `slii_news_feeds` VALUES ('19', 'wewwew', 'wewe');
INSERT INTO `slii_news_feeds` VALUES ('20', '                            asas', 'asasa');
INSERT INTO `slii_news_feeds` VALUES ('21', 'asa', 'aaasasa');
INSERT INTO `slii_news_feeds` VALUES ('22', 'sdddddddddddddd', 'assasa');

-- ----------------------------
-- Table structure for `slii_recent_works`
-- ----------------------------
DROP TABLE IF EXISTS `slii_recent_works`;
CREATE TABLE `slii_recent_works` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded_on` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slii_recent_works
-- ----------------------------
INSERT INTO `slii_recent_works` VALUES ('30', '1.jpg', '1', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_recent_works` VALUES ('31', '2.jpg', '2', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_recent_works` VALUES ('32', '3.jpg', '3', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_recent_works` VALUES ('33', '4.jpg', '4', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_recent_works` VALUES ('51', 'DivisionList.png', '5', '0000-00-00 00:00:00', '0');

-- ----------------------------
-- Table structure for `slii_slider_images`
-- ----------------------------
DROP TABLE IF EXISTS `slii_slider_images`;
CREATE TABLE `slii_slider_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `uploaded_on` datetime NOT NULL,
  `status` enum('1','0') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slii_slider_images
-- ----------------------------
INSERT INTO `slii_slider_images` VALUES ('40', '2.jpg', '', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_slider_images` VALUES ('41', '3.jpg', '', '0000-00-00 00:00:00', '1');
INSERT INTO `slii_slider_images` VALUES ('42', '2.jpg', '', '0000-00-00 00:00:00', '0');
INSERT INTO `slii_slider_images` VALUES ('43', '3.jpg', '', '0000-00-00 00:00:00', '0');
INSERT INTO `slii_slider_images` VALUES ('44', 'Clearbtn_newInquiry.png', '', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for `slii_subjects`
-- ----------------------------
DROP TABLE IF EXISTS `slii_subjects`;
CREATE TABLE `slii_subjects` (
  `subject_code` varchar(10) DEFAULT NULL,
  `fk_course_id` int(11) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_subjects
-- ----------------------------
INSERT INTO `slii_subjects` VALUES ('SL 92', null, '1', 'Insurance Busness & Finance');
INSERT INTO `slii_subjects` VALUES ('SL 05', null, '2', 'Insurance Law');
INSERT INTO `slii_subjects` VALUES ('SL 80', null, '3', 'Underwriting Practice');
INSERT INTO `slii_subjects` VALUES ('SL 86', null, '4', 'Personal Insurance');
INSERT INTO `slii_subjects` VALUES ('IRP', null, '5', 'Insurance Regulatory Practice In Srilanka');
INSERT INTO `slii_subjects` VALUES ('SL 63', null, '6', 'Long Term Insurance Business(Life)');
INSERT INTO `slii_subjects` VALUES ('SL 820', null, '7', 'Advanced Claims(Non Life)');
INSERT INTO `slii_subjects` VALUES ('W01', null, '8', 'Award In General Insurance');
INSERT INTO `slii_subjects` VALUES ('IF3', null, '9', 'Insurance Underiting Process');
INSERT INTO `slii_subjects` VALUES ('IF4', null, '10', 'Insurance Claim Handling Process');
INSERT INTO `slii_subjects` VALUES (null, null, '11', 'ert');
INSERT INTO `slii_subjects` VALUES ('ertert', null, '12', 'ert');
INSERT INTO `slii_subjects` VALUES ('ertret', null, '13', 'ert');

-- ----------------------------
-- Table structure for `slii_user_group_pivot`
-- ----------------------------
DROP TABLE IF EXISTS `slii_user_group_pivot`;
CREATE TABLE `slii_user_group_pivot` (
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_group_id` int(11) DEFAULT NULL,
  `user_group_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`user_group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_user_group_pivot
-- ----------------------------
INSERT INTO `slii_user_group_pivot` VALUES ('1', '1', '1');
INSERT INTO `slii_user_group_pivot` VALUES ('6', '2', '2');
INSERT INTO `slii_user_group_pivot` VALUES ('3', '5', '3');
INSERT INTO `slii_user_group_pivot` VALUES ('7', '5', '4');
INSERT INTO `slii_user_group_pivot` VALUES ('8', '1', '7');
INSERT INTO `slii_user_group_pivot` VALUES ('37', '5', '33');
INSERT INTO `slii_user_group_pivot` VALUES ('38', '5', '34');
INSERT INTO `slii_user_group_pivot` VALUES ('39', '5', '35');
INSERT INTO `slii_user_group_pivot` VALUES ('40', '5', '36');
INSERT INTO `slii_user_group_pivot` VALUES ('41', '5', '37');
INSERT INTO `slii_user_group_pivot` VALUES ('42', '5', '38');

-- ----------------------------
-- Table structure for `slii_users`
-- ----------------------------
DROP TABLE IF EXISTS `slii_users`;
CREATE TABLE `slii_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `payment_type` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of slii_users
-- ----------------------------
INSERT INTO `slii_users` VALUES ('1', 'AIA', '202cb962ac59075b964b07152d234b70', '1', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('3', 'Member2', '202cb962ac59075b964b07152d234b70', '1', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('6', 'Admin', '202cb962ac59075b964b07152d234b70', '', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('7', 'Member1', '202cb962ac59075b964b07152d234b70', '2', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('8', 'Allianz', '202cb962ac59075b964b07152d234b70', '2', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('37', '123', '202cb962ac59075b964b07152d234b70', '', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('38', '1234', '202cb962ac59075b964b07152d234b70', '', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('39', 'sdfsd', '202cb962ac59075b964b07152d234b70', '3', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('40', '111111111', '202cb962ac59075b964b07152d234b70', '1', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('41', '454', '202cb962ac59075b964b07152d234b70', '', '0000-00-00 00:00:00', null, '1', null);
INSERT INTO `slii_users` VALUES ('42', 'udeep', '202cb962ac59075b964b07152d234b70', '2', '0000-00-00 00:00:00', null, '0', null);

-- ----------------------------
-- Table structure for `slii_videos`
-- ----------------------------
DROP TABLE IF EXISTS `slii_videos`;
CREATE TABLE `slii_videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `video_url` varchar(100) DEFAULT NULL,
  `video_type` varchar(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slii_videos
-- ----------------------------
INSERT INTO `slii_videos` VALUES ('2', 'http://www.youtube.com/embed/W-Q7RMpINVo', null, '0');
INSERT INTO `slii_videos` VALUES ('3', 'http://www.youtube.com/embed/W-Q7RMpINVo', null, '0');
INSERT INTO `slii_videos` VALUES ('8', 'https://www.youtube.com/embed/J6vIS8jb6Fs', null, '1');
