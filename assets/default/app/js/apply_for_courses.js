var check_subjects = [];

$(function () {
});

function apply_for_courses() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"member/assign_for_courses",
        data: {
            'course_id' : $('#course_apply').val(),
            'assign_subjects' : check_subjects
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

$('#course_apply').on('change', function (e) {
    var course_id = this.value;
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"member/get_subjects_for_course",
        data: {
            'course_id' : course_id,
        },
        success: function(response) {
            var html = '';
            $.each(response, function( index, value ) {
                var subject_id = value['id'];
                var subject = value['subject_name'];
                var subject_code = value['subject_code'];
                var is_compulsory = value['is_compulsory'];
                var is_disabled = '';
                var is_checked = '';
                if(is_compulsory == 1){
                    is_disabled = 'disabled';
                    is_checked = 'checked';
                    check_subjects.push(subject_id);
                }else{
                    is_disabled = '';
                    is_checked = '';
                }
                html = html + '<tr><td>'+subject+'</td> <td>'+subject_code+'</td> <td> <input data-subject-id = '+subject_id+' '+is_disabled+' '+is_checked+' name="assign-subject" class="minimal" type="checkbox"> </td>  <tr> </div>';
            });
            $('#subject_list').html(html);
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass   : 'iradio_minimal-blue'
            });

            $('input[name="assign-subject"]').on('ifClicked', function (event) {
                var subject_id = $(this).attr('data-subject-id');
                var index =  check_subjects.indexOf(subject_id);
                if( index > -1 ){
                    check_subjects.splice(index , 1);
                }else{
                    check_subjects.push(subject_id);
                }
            });
        }
    });
});

$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
});



