function add_service() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_services",
        data: {
            'description' : $('#description').val(),
            'title' : $('#title').val(),
            'icon' : $('#icon').val(),
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

var table = $('#slii_services_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_all_services',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "title"},
        {"data": "description"},
        {"data": "icon"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<a data-id="'+row.id+'" class="btn btn-xs btn-danger btn-delete">Delete</a>';
                return buttons;
            }
        }
    ]
});


$('#slii_services_tbl').on( 'click', '.btn-delete' , function () {
    var id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/delete_service",
        data: {
            'id' : id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
});