var table = $('#course_payment_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_course_payment',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "username"},
        {"data": "course_name"},
        {"data": "course_fee"},
        {"data": "outstanding"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<button data-course-payment-id = "'+row.id+'" class="btn btn-primary btn-xs btn-payment">Payments</button>';
                return buttons;
            }
        }
    ]
});

$('#course_payment_tbl').on( 'click', '.btn-payment' , function () {
    $('#course_payment_model').modal('show');
    var course_payment_id = $(this).attr('data-course-payment-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/get_payments_for_course",
        data: {
            'id' : course_payment_id,
        },
        success: function(response) {
            var html = '';
            $.each(response, function( index, exp_obj ) {
                html = html +'<tr><td>'+exp_obj.amount+'</td><tr>';
            });
            $('#course_payment_details').html(html);
        }
    });
});
