var table = $('#albums_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_slii_albums',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "album_name"},
        {"data": "album_image",
            "render": function(data, type, row, meta){
                var image = '<img class="album-img" src=" '+CCApp.base_url+'assets/default/img/front/albums/'+data+'" />';

                return image;
            }
        },
        {"data": "status" , 'visible' :false},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = ' <a data-id="'+row.id+'" class="btn btn-xs btn-danger btn-delete">Delete</a>';
                if( row.status == 1 ){
                    var buttons = buttons  + ' <a class="btn btn-xs btn-success" onclick="set_active_album('+row.id+',0)">InActive</a>';
                }else{
                    var buttons = buttons + ' <a class="btn btn-xs btn-primary" onclick="set_active_album('+row.id+',1)">Active</a>';
                }
                return buttons;
            }
        }
    ]
});


$('#albums_tbl').on( 'click', '.btn-delete' , function () {
    var album_id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/delete_album",
        data: {
            'album_id' : album_id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
});

function set_active_album(id , type) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/set_active_album",
        data: {
            'id' : id,
            'type' : type
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}