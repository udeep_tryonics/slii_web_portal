function album_clk(id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/get_album_images",
        data: {
            'id' : id,
        },
        success: function(response) {
            var html = '';
            $.each( response, function( key, value ) {
                var file_name = value.file_name;
                var title = value.title;
                var image_url = CCApp.base_url + 'assets/default/img/front/gallery/' + file_name;
                html = html + "<li class='item-thumbs col-lg-3 design' data-id='id-0' data-type='web'>" +
                    "                                <a class='hover-wrap fancybox' data-fancybox-group='gallery' title='' href='"+image_url+"'>" +
                    "                                    <span class='overlay-img'></span>\n" +
                    "                                    <span class='overlay-img-thumb'><i class='icon-info-blocks fa fa-code'></i></span>" +
                    "                                </a>\n" +
                    "                                <img src='"+image_url+"' alt=''>" +
                    "                            </li>";
            });
            $('#thumbs').html(html);

            $('#back_to_albums').html('<button class="btn btn-default" onclick="back_to_albums()">Back</button>');
        }
    });

}

function back_to_albums() {
    window.location.href = CCApp.base_url + 'portfolio';
}