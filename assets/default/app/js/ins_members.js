var table = $('#user_mgt').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'ins_comp/get_ins_users_list',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "username"},
        {"data": "slii_member_id"},
        {"data": "is_active"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<a href="'+CCApp.base_url+'/ins_comp/add_members?id='+row.slii_member_id+'" class="btn btn-xs btn-success btn-edit">Edit</a>';
                if(row.is_active == 0){
                    buttons = buttons + '<a data-main-id='+row.id+' class="btn btn-xs btn-primary btn-activate">Activate</a>';
                }
                return buttons;
            }
        }
    ]
});

$('#user_mgt').on( 'click', '.btn-activate' , function () {
    var member_id = $(this).attr('data-main-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/activate_user",
        data: {
            'id' : member_id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
});



