var table = $('#slii_subjects_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_slii_subjects',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "subject_name"},
        {"data": "subject_code"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<a class="btn btn-xs btn-success btn-edit">Edit</a>';
                return buttons;
            }
        }
    ]
});

function add_subject() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_subject",
        data: {
            'subject_name' : $('#subject_name').val(),
            'subject_code' : $('#subject_code').val(),
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}