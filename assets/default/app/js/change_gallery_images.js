function remove_gallery_img(id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/remove_gallery_img",
        data: {
            'image_id' : id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

function update_gallery_img(id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/update_gallery_img",
        data: {
            'image_id' : id,
            'title' :  $('#title'+id).val(),
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

function set_active_image(id , status) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/set_active_image",
        data: {
            'image_id' : id,
            'status' : status
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

