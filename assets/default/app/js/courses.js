var table = $('#slii_courses_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_slii_courses',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "course_name"},
        {"data": "course_description"},
        {"data": "subjects"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<a data-id="'+row.id+'" data-course_name="'+row.course_name+'"  data-course_description="'+row.course_description+'" data-subjects="'+row.subjects+'" class="btn btn-xs btn-success btn-edit">Edit</a>';
                return buttons;
            }
        }
    ]
});

jQuery(".chzn-select").chosen();

function add_course(){
    var course_subjects = $('#course_subjects').val();
    var course_name = $('#course_name').val();
    var course_code = $('#course_code').val();
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_course",
        data: {
            'course_subjects' : course_subjects,
            'course_name' : course_name,
            'course_code' : course_code,
            'course_id' : $('#course_id_hidden').val()
        },
        success: function(response) {
            $('#course_id_hidden').val('');
            $('#add-course').text('Add');
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

$('#slii_courses_tbl').on( 'click', '.btn-edit' , function () {
    $('#add-course').text('Edit');
    $('#course_name').val($(this).attr('data-course_name'));
    $('#course_code').val($(this).attr('data-course_description'));
    var subjects = JSON.parse("[" + $(this).attr('data-subjects') + "]");
    $('#course_subjects').val(subjects);
    $('#course_subjects').trigger("chosen:updated");
    $('#course_id_hidden').val($(this).attr('data-id'));
});



