$('#contactSuccess').hide();
$('#contactError').hide();

$("#add_contat_details").click(function(event) {
    var name = $('#name').val();
    var subject = $('#subject').val();
    var message = $('#message').val();
    var email = $('#email').val();

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_contact_details",
        data: {
            'name' : name,
            'subject' : subject,
            'message' : message,
            'email' : email,
        },
        success: function(response) {
            if(response.type == 'success'){
                $('#contactSuccess').addClass('alert alert-success');
                $('#contactSuccess').html("<strong>Success!</strong> "+response.message+"").show().delay(4000).fadeOut();
                $('.contact').val('');
            }else{
                $('#contactError').addClass('alert alert-danger');
                $('#contactError').html("<strong>Error!</strong> "+response.message+"").show().delay(4000).fadeOut();
            }
        }
    });
});