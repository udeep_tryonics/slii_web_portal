var table = $('#user_mgt').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_users_list',
        "type": 'post',
    },
    "pageLength": 10,
    "columns": [
        {"data": "id"},
        {"data": "username"},
        {"data": "description"},
        {"data": "company"},
        {"data": "is_active"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<button data-main-id="'+row.id+'" class="btn btn-xs btn-success btn-edit">Edit</button>' +
                    '<button data-main-id="\'+row.id+\'" class="btn btn-xs btn-danger btn-ubl">Block</button>' +
                    '<button data-main-id="\'+row.id+\'" class="btn btn-xs btn-info btn-history">History</button>';
                if(row.is_active == 0){
                    buttons = buttons + '<a data-main-id="'+row.id+'" class="btn btn-xs btn-primary btn-activate">Activate</a>';
                }
                return buttons;
            }
        }
    ]
});

$('#user_mgt').on( 'click', '.btn-activate' , function () {
    var member_id = $(this).attr('data-main-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/activate_user",
        data: {
            'id' : member_id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
});


