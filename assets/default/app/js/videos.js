function add_video() {
    var video_url = $('#video_url').val();

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_video",
        data: {
            'video_url' : video_url,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            setTimeout(function(){ location.reload(); }, 3000);

        }
    });
}

function set_active_video(id , status) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/set_active_video",
        data: {
            'video_id' : id,
            'status' : status
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

function remove_video(id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/remove_video",
        data: {
            'video_id' : id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}
