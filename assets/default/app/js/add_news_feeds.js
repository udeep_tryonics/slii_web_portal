var table = $('#news_feed_tbl').DataTable({
    "order": [[0, "desc"]],
    "processing": true,
    "serverSide": true,
    "responsive":true,
    "ajax": {
        "url": CCApp.base_url+'admin/get_news_feeds',
        "type": 'post',
    },
    "pageLength": 5,
    "columns": [
        {"data": "id"},
        {"data": "feed"},
        {"data": "title"},
        {"data": "action",
            "searchable": false,
            "sortable": false,
            "render": function(data, type, row, meta){
                var buttons = '<button data-id="'+row.id+'" data-title="'+row.title+'" data-feed="'+row.feed+'" class="btn btn-xs btn-success btn-edit-feed">Edit</button>' +
                    ' ' +  '<button data-id="'+row.id+'" class="btn btn-xs btn-danger btn-delete-feed">Delete</button>';
                return buttons;
            }
        }
    ]
});

function add_feed() {
    var feed = $('#news_feed').val();
    var title = $('#news_title').val();
    var hidden_input_feed = $('#hidden_input_feed').val();

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/add_feed",
        data: {
            'feed' : feed,
            'title' : title,
            'feed_id' : $('#hidden_input_feed').val()
        },
        success: function(response) {
            set_msg(response.type,response.message);
            table.ajax.reload();
            $('#add_news_feed').text('Add');
            $('#hidden_input_feed').val('');
            $('#news_feed').val('');
            $('#news_title').val('');
        }
    });
}

$('#news_feed_tbl').on( 'click', '.btn-edit-feed' , function () {
    var feed_id = $(this).attr('data-id');
    var feed = $(this).attr('data-feed');
    var title = $(this).attr('data-title');
    $('#hidden_input_feed').val(feed_id);
    $('#news_feed').val(feed);
    $('#news_title').val(title);
    $('#add_news_feed').text('Update');
});

$('#news_feed_tbl').on( 'click', '.btn-delete-feed' , function () {
    var feed_id = $(this).attr('data-id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/delete_news_feed",
        data: {
            'feed_id' : feed_id
        },
        success: function(response) {
            set_msg(response.type,response.message);
            table.ajax.reload();
        }
    });
});







