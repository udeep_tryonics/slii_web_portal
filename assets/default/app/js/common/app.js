/**
 * Created by ruwan on 10/6/17.
 */
function set_msg(type,msg){
    _set_msg(type,msg)
}
//refactor code
function _set_msg(type,msg){
    var cls='';
    var icon='';
    var sharp='';
    if(msg.length > 0){
        if(type == 'success') {
            cls = "alert-success";
            icon = "fa-check";
            sharp = "Done";
        }else if(type == 'warning'){
            cls = "alert-warning";
            icon = "fa-warning";
            sharp = "Warning";
        }else{
            cls = "alert-danger";
            icon = "fa-ban";
            sharp = "Opps";
        }
    var message = '<div class="alert '+cls+' alert-dismissible" id="cc-alerts">'+
                    '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                    '<h4><i class="icon fa '+icon+'"></i>' +sharp+'!</h4>'+
                        msg+
                  '</div>';
    }
    jQuery(message).prependTo('.content').show().delay(5000).fadeOut();
}


jQuery(document).ready(function(){
    jQuery('#cc-alerts').show().delay(5000).fadeOut();
});

