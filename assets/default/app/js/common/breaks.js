/* 
 * @author gayani
 * break start and finish
 */

$(document).on('change', '#break_list', function () {
    var break_id = $(this).val();
    check_track(break_id);
});
      
function check_track(id)
{

    $.ajax({
        url : CCApp.base_url+'agent/tracker_status',
        type: 'post',
        data: {break_id:id},
        dataType:'json',
        success:function(res){
            //location.reload();
            
        }
    });
}


//when click start button
$('#start').on('click',function(e){
    e.preventDefault();
   $.ajax({
        url : CCApp.base_url+'agent/save_tracker',
        type: 'post',
        data: {break_id:$('#break_list').val()},
        dataType:'json',
        success:function(res){
            if(res.type == "success"){
                window.location.reload(true);
            }else{
                set_msg(res.type,res.message);
            }
        }
    });
});

//when click finish button
$('#finish').on('click',function()
{
     $.ajax({
        url : CCApp.base_url+'agent/update_tracker',
        type: 'post',
        data: {record_id:$('#finish').val()},
        dataType:'json',
        success:function(res){
            if(res.type == "success"){
                window.location.reload(true);
            }else{
                set_msg(res.type,res.message);
            }
        }
    });
});


      
