jQuery('.notification_read_link').on('click',function(e){
    var notification_id = $(this).attr('data-notification-id');
    var notification_link = $(this).attr('data-notification-link');
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url + "agent/set_notification_read",
        data: {
            'notification_id': notification_id
        },
        success: function (data) {
            window.location.href = notification_link;
        }
    });
});

