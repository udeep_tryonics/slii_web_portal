$("[data-fancybox]").fancybox({ });

function remove_slider_img(id) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/remove_slider_img",
        data: {
            'image_id' : id,
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}

function set_active_slider(id , status) {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"admin/set_active_slider",
        data: {
            'image_id' : id,
            'status' : status
        },
        success: function(response) {
            set_msg(response.type,response.message);
            location.reload();
        }
    });
}