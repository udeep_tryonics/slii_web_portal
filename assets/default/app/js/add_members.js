draw_exp_tbl();

function submit_member_data() {

    var edu_qualifications = new Array();
    $.each($("input[name='edu_qualifications[]']:checked"), function() {
        edu_qualifications.push($(this).val());
    });

    var ins_areas = new Array();
    $.each($("input[name='ins_areas[]']:checked"), function() {
        ins_areas.push($(this).val());
    });

    var current_industry_nature = new Array();
    $.each($("input[name='current_industry_nature[]']:checked"), function() {
        current_industry_nature.push($(this).val());
    });

    var insurance_type = new Array();
    $.each($("input[name='insurance_type[]']:checked"), function() {
        insurance_type.push($(this).val());
    });

    var pref_mail_adr = new Array();
    $.each($("input[name='pref_mail_adr[]']:checked"), function() {
        pref_mail_adr.push($(this).val());
    });

    var surname = new Array();
    $.each($("input[name='surname[]']:checked"), function() {
        surname.push($(this).val());
    });

    $.ajax({
        type: "POST",
        dataType: 'json',
        url: CCApp.base_url+"ins_comp/member_registration_submit",
        data: {
            'fullname' : $('#fullname').val(),
            'surname' : surname,
            'dob' : $('#dob').val(),
            'nic' : $('#nic').val(),
            'cmp_name_adr' : $('#cmp_name_adr').val(),
            'cmp_phone' : $('#cmp_phone').val(),
            'cmp_email' : $('#cmp_email').val(),
            'home_adr' : $('#home_adr').val(),
            'home_phone' : $('#home_phone').val(),
            'home_mobile' : $('#home_mobile').val(),
            'home_email' : $('#home_email').val(),
            'present_occupation' : $('#present_occupation').val(),
            'edu_qualifications' : edu_qualifications,
            'ins_areas' : ins_areas,
            'current_industry_nature' : current_industry_nature,
            'insurance_type' : insurance_type,
            'pref_mail_adr' : pref_mail_adr,
            'home_mobile' : $('#home_mobile').val(),
            'slii_member_edit_id' : $('#slii_member_edit_id').val(),
            'experience_list' : exp_array
        },
        success: function(response) {
            $('#slii_member_edit_id').val('');
            set_msg(response.type,response.message);
            //location.reload();
        }
    });
}

$(function () {
    $('.datepicker').datepicker({
        autoclose: true
    });
});

$('[data-mask]').inputmask();

$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass   : 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
});


function add_experience() {
    var exp_name_of_company = $('#exp_name_of_company').val();
    var exp_job_title = $('#exp_job_title').val();
    var exp_from = $('#exp_from').val();
    var exp_to = $('#exp_to').val();
    var exp_description = $('#exp_description').val();
    var exp_obj = {};
    exp_obj.name_of_company = exp_name_of_company;
    exp_obj.job_title = exp_job_title;
    exp_obj.exp_from = exp_from;
    exp_obj.exp_to = exp_to;
    exp_obj.description_of_job = exp_description;
    exp_array.push(exp_obj);
    draw_exp_tbl();
}

function draw_exp_tbl() {

    var tb_html = '';
    var i = 0;
    $.each(exp_array, function( index, exp_obj ) {
        tb_html = tb_html + '<tr> <td>'+exp_obj.name_of_company+'</td> <td>'+exp_obj.job_title+'</td> <td>'+exp_obj.exp_from+'</td> <td>'+exp_obj.exp_to+'</td>  <td><button class="btn btn-danger btn-xs" onclick="delete_exp('+i+')">delete</button></button></td>   </tr>';
        i++;
    });
    $('#exp_tbl_body').html(tb_html);
}

function delete_exp(index) {
   exp_array.splice(index , 1);
   draw_exp_tbl();
}