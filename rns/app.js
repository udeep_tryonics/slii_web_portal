var express = require('express');
var app = express();
var path = require('path');
var http = require('http');

var server = http.createServer(app).listen(3000,function(){
console.log("listning to 3000");
});
var io = require('socket.io').listen(server);


var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/', function(req, res) {
   res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
	console.log('A user connected');
    socket.on('subscribe', function(room) { 
        console.log('joining room', room);
        socket.join(room); 
    })
});

app.post('/signal', function(req, res) {
	console.log(req.body.rooms);
 
	req.body.rooms.forEach(function(val, key){
		//io.sockets.join(val);
		io.sockets.in(val).emit('signal', req.body.response);
		//io.emit(val,req.body.response);
	});
	res.end('OK');
});

