<?php
/**
 * Title           Title
 *
 * @package        Tryonics-callcenter
 * Location        utility_helper.php
 *
 * @author         ruwan - <ruwan@tryonics.com>
 * @copyright      Tryonics (Pvt) Ltd
 *
 * created on      9/28/17, 11:25 AM by ruwan
 *
 * Description     Utility helper functions
 *
 * */
//add assets
if(!function_exists('add_css')){
    function add_asset($files=array())
    {
        $ci = &get_instance();
        if(is_array($files) && !empty($files)){
            foreach($files AS $section=>$item){
                if(in_array($section,array('header_js','footer_js','header_css','footer_css'))){
                    $ci->config->set_item($section,$item);
                }
            }
        }
    }
}

//load assets
if(!function_exists('load_css_js')){
    function load_css_js($section='')
    {   $str = '';
        $ci = &get_instance();
        $css = $ci->config->item($section.'_css');
        $js  = $ci->config->item($section.'_js');
        foreach($css AS $item){
            $str .= '<link rel="stylesheet" href="'.base_url().'assets/'.THEME.$item.'" type="text/css" />'."\n";
        }
        foreach($js AS $item){
            $str .= '<script type="text/javascript" src="'.base_url().'assets/'.THEME.$item.'"></script>'."\n";
        }
        return $str;
    }
}

if(!function_exists('rns_base_url')){
    function rns_base_url($path=null)
    {   $str = '';
        $ci = &get_instance();
        $ci->load->config('app_config', TRUE);
        $rns_base_url = $ci->config->item('rns_base_url', 'app_config');
        if($path){
            $rns_base_url .= $path;
        }
        return $rns_base_url;
    }
}

//calculate elapsed time of notification
if(!function_exists('calc_diff_elapsed_notification')) {
    function calc_diff_elapsed_notification($start_time)
    {
        $date = new DateTime(null, new DateTimeZone('Asia/Colombo'));
        $date_now = $date->format('Y-m-d H:i:s');
        $date_c = new DateTime($date_now);
        $interval = $start_time->diff($date_c);
        $elapsed_hrs = $interval->format('%h');
        $elapsed_mins = $interval->format('%i');
        if($elapsed_hrs == 0){
            $elapsed =    $elapsed_mins . ' mins';
        }else{
            $elapsed =   $elapsed_hrs . ' hrs ' . $elapsed_mins . ' mins';
        }
        if( $start_time < $date_c ){
            $elapsed = $elapsed . ' ago';
        }
        else{
            $elapsed = 'from ' . $elapsed;
        }

        return $elapsed;
    }
}