<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Title           Auth Library
 *
 * @package        Tryonics
 * Location        application/libraries/AuthLib.php
 *
 * @author         ruwan - <ruwanpathmalal@gmail.com>
 * @copyright      Spark
 *
 * created on      27/09/2017, 12:21 PM by ruwan
 *
 * Description     Consist of common authentication handling methods
 *
 * */

class AuthLib {
    /*
     * ci object
     */
    protected $ci;

    /**
     * __construct
     *
     * @param none
     * @access public
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    function __construct(){
        $this->ci = &get_instance();
        $this->ci->load->model('Auth_model');
    }

    /**
     * sign in method
     * @access public
     * @param string $username
     * @param string $password
     * @return String
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function signin($username,$password){
        $data = $this->ci->Auth_model->get_user_by_username_password($username,$password);
        //status returned : login success full -1, user doesn't exist - false
        return $data;
    }

    /**
     * Generate password hash with salt
     * @access public
     * @param string $password
     * @return String
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function hash_password($password){
        return sha1($password);
    }

    /**
     * init user session
     * @access public
     * @param mixed $user
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function init_user_session($user){
        $login_data = array('id'=>$user['id'],
                        'username'=>$user['username'],
                        'email'=>$user['email'],
                        'company'=>$user['company'],
                        'companyname'=>$user['companyname'],
                        'roles'=>$this->ci->Auth_model->get_user_assigned_group($user['id'])
            );
        $this->ci->session->set_userdata('login_data',$login_data);
    }

    /**
     * check user logged or not to the system.
     * @access public
     * @return bool true or false
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function is_user_logged(){
        return $this->ci->session->has_userdata('login_data');
    }

    /**
     * unset session data
     * @access public
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function unset_user_data(){
        $this->ci->session->unset_userdata('login_data');
    }

    /**
     * logout user
     * @access public
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function logout_user(){
        $this->unset_user_data();
    }

    /**
     * check user has rights. return status.
     * @access public
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function check_has_rights($role){
        if(!$this->is_user_logged()){
            return false;
        }else{
            $user_data = $this->ci->session->userdata('login_data');
            return in_array(strtolower($role),$user_data['roles']);
        }
    }

    /**
     * has rights. and redirected to relevant page
     * @access public
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function has_rights($role){
        if(!$this->is_user_logged()){
            redirect('user/auth');
        }else{
            if(!$this->check_has_rights($role)){
                //set flash message to user
                $this->user_landing();
            }
        }

    }

    /**
     * route user to specific landing pages
     * @access public
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function user_landing(){
        // implement logic to user landing
        if($this->check_has_rights('SLII_ADMIN')){
            redirect('admin/profile');
        }elseif($this->check_has_rights('INSURANCE_COMPANY')){
            redirect('ins_comp/profile');
        }elseif($this->check_has_rights('SLII_MEMBER')){
            redirect('member/profile');
        }
    }

    /**
     * Update current session.
     * @access public
     * @param int store_id
     * @return void
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function update_session($key,$value){
        $login_data = $this->ci->session->userdata('login_data');
        $login_data[$key] = $value;
        $this->ci->session->set_userdata('login_data',$login_data);
    }

    /**
     * Update.
     * @access public
     * @param mixed $data
     * @param mixed $where
     * @return int|bool
     * @author ruwan - <ruwanpathmalal@gmail.com>
     * */
    public function update($data,$where){
        return $this->ci->Auth_model->update($data,$where);
    }

    /**
     * Reset Password.
     * @access public
     * @return int|bool
     * @author udeep - <udeepharsha@gmail.com>
     * */
    public function reset_password_user($new_pw,$user_name){
        return $this->ci->Auth_model->reset_password_user($new_pw,$user_name);
    }

}