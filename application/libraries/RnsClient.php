<?php
/**
 * Title           Title
 *
 * @package        Tryonics-callcenter
 * Location        RnsClient.php
 *
 * @author         ruwan - <ruwan@tryonics.com>
 * @copyright      Tryonics (Pvt) Ltd
 *
 * created on      10/16/17, 3:06 PM by ruwan
 *
 * Description     short description
 *
 * */

class RnsClient {
    protected $_data = array();
    protected $_url = '';
    protected $_option = array();

    public function __construct(){
        $this->_url = rns_base_url();
    }

    public function set_endpoint($endpoint){
        $this->_url .= $endpoint;
        return $this;
    }
    public function set_data($array){
        if(!empty($array)){
            $this->_data = $array;
        }
        return $this;
    }
    public function get(){
        $this->_option = array('http' =>
            array(
                'method'  => 'GET',
                'header'  => 'APIKEY: QWERTYUIOP/#$ASDFGHJKL',
            )
        );
        $this->_url .="?".http_build_query($this->_data);
        return $this->send();
    }

    public function post(){
        $this->_option = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => "Content-type: application/json\r\n".
                'APIKEY: QWERTYUIOP/#$ASDFGHJKL'."\r\n",
                'content' => json_encode($this->_data)
            )
        );
        return $this->send();
    }

    public function reset(){
        return new RnsClient();
    }

    private function send(){
        $context  = stream_context_create($this->_option);
        return file_get_contents($this->_url,false,$context);
    }

}