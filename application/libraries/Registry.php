<?php
/**
 * Title           Registry Class
 *
 * @package        Tryonics-callcenter
 * Location        Registry.php
 *
 * @author         ruwan - <ruwan@tryonics.com>
 * @copyright      Tryonics (Pvt) Ltd
 *
 * created on      10/16/17, 3:27 PM by ruwan
 *
 * Description     Share Instance through controller views etc.
 *
 * */

class Registry
{

    /**
     * @var array
     */
    public static $storedValues = array();

    /**
     * @param string $key
     * @param mixed  $value
     *
     * @return void
     */
    public static function set($key, $value)
    {
        self::$storedValues[$key] = $value;
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public static function get($key)
    {
        if(array_key_exists($key,self::$storedValues)){
            return self::$storedValues[$key];
        }
        return false;

    }
}