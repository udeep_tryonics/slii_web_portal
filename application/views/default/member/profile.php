<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SLII Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="row">
            <div class="col-md-3">
                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/'.THEME);?>app/images/default_icons/call_man.png" alt="User profile picture">

                        <h3 class="profile-username text-center"><?php echo $this->session->userdata('login_data')['username'] ?></h3>

                        <p class="text-muted text-center"><?php echo $this->session->userdata('login_data')['companyname'] ?></p>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="col-md-9">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#courses" data-toggle="tab">Courses</a></li>
                        <li><a href="#downloads" data-toggle="tab">Downloads</a></li>
                        <li><a href="#payments" data-toggle="tab">Payments</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="active tab-pane" id="courses">
                            <ul >
                                <?php
                                    foreach ($courses as $course){
                                        echo '<li class="class="treeview">'.$course['course_name'].'</li>';
                                        $subjects = $course['subjects'];
                                        $compulsory_subjects = $course['compulsory_subjects'];
                                        echo "<ul>";
                                        foreach ($subjects as $subject){
                                            if( in_array($subject['id'] ,$compulsory_subjects ) ){
                                                echo '<li class="compulsory-blue">'.$subject['name'].'  (Compulsory)</li>';
                                            }else{
                                                echo '<li>'.$subject['name'].'</li>';
                                            }
                                        }
                                        echo "</ul>";
                                    }
                                    if( empty($courses) ){
                                        echo '<label class="alert alert-warning">No Assign Courses</label>';
                                    }
                                ?>
                            </ul>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="downloads">
                            <ul >

                                <?php
                                    foreach ($courses as $course){
                                    echo '<li class="class="treeview">'.$course['course_name'].' <button class="btn btn-xs btn-primary">Download</button>    </li>';
                                    $subjects = $course['subjects'];
                                    echo "<ul>";
                                    foreach ($subjects as $subject){
                                        echo '<li>'.$subject['name'].'<button class="btn btn-xs btn-primary">Download</button></li>';
                                    }
                                    echo "</ul>";
                                }

                                if( empty($courses) ){
                                    echo '<label class="alert alert-warning">No Downloads</label>';
                                }
                                ?>
                            </ul>
                        </div>
                        <!-- /.tab-pane -->
                        <div class="tab-pane" id="payments">
                            Payments
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>


