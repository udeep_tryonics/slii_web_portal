<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Apply for course
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="col-sm-12">
                    <div class="col-sm-4">
                        <label>Course</label>
                        <select class="form-control" id="course_apply">
                            <option disabled="disabled" selected="selected">--Select--</option>
                            <?php
                                foreach ($courses as $course){
                                    echo '<option value='.$course['id'].'>'.$course['course_name'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-sm-8">
                        <form>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-6">
                                        <label>Full Name</label>
                                        <input class="form-control" id="full_name"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Date Of Birth</label>
                                        <input class="form-control" id="dob"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Nic Number</label>
                                        <input class="form-control" id="nic"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Home Adress</label>
                                        <input class="form-control" id="home_adress"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Home Telephone</label>
                                        <input class="form-control" id="home_telephone"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Mobile</label>
                                        <input class="form-control" id="mobile"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Name Of Employer</label>
                                        <input class="form-control" id="name_of_employer"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Adress</label>
                                        <input class="form-control" id="adress"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Telephone And Fax</label>
                                        <input class="form-control" id="telephone_fax"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>Email</label>
                                        <input class="form-control" id="email"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>SLII Membership Number</label>
                                        <input class="form-control" id="membership_no"/>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>CII Pin No</label>
                                        <input class="form-control" id="cii_pin"/>
                                    </div>
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <td>Subject</td>
                                            <td>Subject Code</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody id="subject_list">
                                    </tbody>
                                </table>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
                <br>
                <br>
                <div class="col-sm-12">
                    <div class="col-sm-12">
                        <button class="btn btn-info btn-sm pull-right" onclick="apply_for_courses()">Apply</button>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!-- /.content -->
</div>


