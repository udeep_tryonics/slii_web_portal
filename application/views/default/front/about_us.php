<section id="content">
    <div class="container" style="padding-top: 65px;">
        <div class="about">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-logo">
                        <h3>About Us<span class="color"></span></h3>
                        <p>Our journey began in October 1981, when a group of 14 members gathered at a private residence with a common interest - to build an educational platform that represents the insurance industry in Sri Lanka and formulate a suitable constitution that served to regulate member activities. This two-fold strategy soon grew to fruition in February 1982, when the first-ever inaugural meeting was conducted, following months of successful preliminary work. Today, we are an active body, supporting the unified effort to develop the insurance industry in Sri Lanka. Our objective is to operate as a central organization that would strive in enhancing the efficiency, progress and general development amongst personnel engaged or employed in insurance.</p>
                        <p>
                            Driven by an enduring commitment to enhance the scope of insurance in Sri Lanka, we have forged several affiliations over the years with recognized international bodies, including The Chartered Insurance Institute of London and the Insurance Institute of India to offer a broad range of recognized qualifications at Certificate, Diploma and Advanced Diploma levels. In partnering with distinguished educational entities, we pursue professional excellence, along with the continuous support of the Insurance Board of Sri Lanka as well as leading insurance companies and broker organizations in the country.</p>
                    </div>
                </div>
            </div>
            <br>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="about-logo">
                        <h3>Our Mission<span class="color"></span></h3>
                        <p>To enhance professionalism in the practice of insurance in Sri Lanka, and develop the necessary human resources in insurance and other related financial services. </p>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row">

                <div class="col-md-6">
                    <h3>About The Institue <span class="color"></span></h3>
                    <p>The Sri Lanka Insurance Institute is the only educational body for those in the local
                        insurance field. It was established in 1981 to enhance professionalism in the practice of
                        insurance in Sri Lanka and develop the necessary human resources in insurance and other
                        related financial services. Over the years the institute has been governed by various
                        councils and now comprises of 11 dynamic individuals from the field of insurance. The
                        institute was established to fulfill the need to create and autonomous educational body
                        devoted to enhancing the standards of the insurance field in Sri Lanka.</p>
                    <p>	The institute conducts examinations for the Chartered Insurance Institute and Insurance
                        Institute of India in addition to conducting local examinations.</p>
                    <p>The certificate, Diploma and Associate examinations are held for The Chartered
                        Insurance Institute twice a year and Licentiate, Associate and Fellowship examinations
                        for the Indian Insurance Institute bi-annually. Local examinations held for the award of
                        certificates are namely Insurance Foundation Certificate and Certificate in Insurance
                        Practice. We have recently begun to conduct classes for the CII (Award) – Award in
                        Financial Planning.</p>
                    <p>	At the institute we also conduct examinations on behalf of the local governing body,
                        according to the standards set out by them to ensure that proper standards are maintained
                        in the local industry in the area of sales.</p>
                    <p>	We currently have a membership base of around 2500 approximately, which includes
                        honorary members &amp; ordinary members.</p>

                </div>
                <div class="col-md-6">
                    <br><br><br>
                    <img src="<?php echo base_url('assets/'.THEME);?>img/front/bulid.jpg" alt="" />
                    <div class="space"></div>
                </div>
            </div>
            <br/>
            <hr>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <div class="space"></div>
                    <br>
                    <img src="<?php echo base_url('assets/'.THEME);?>img/front/section-image-1.png" alt="" />

                </div>
                <div class="col-md-6">
                    <h3>COUNCIL OF THE YEAR <span class="color">2017 – 2018 </span></h3>
                    <p>President - Mrs N W De A Guneratne<br>
                        Vice President - Mr D P Lokuarachchi<br>
                        Secretary - Mrs K S S Dassenaike<br>
                        Asst. Secretary - Mr U T B Kiridena<br>
                        Treasury - Mr M J L Caspersz<br>
                        Council Members - Mr R Weeraratne<br>
                        Mr S R M Keerthiratne<br>
                        Mr A J Alles<br>
                        Mr P D M Cooray<br>
                        Mr H E Gunawardhena<br>
                        members A N Seneviratne</p>
                    <br>
                    CEO - Mr Udeni Kiridena
                    <p>


                </div>

            </div>
            <br/>
            <hr>
            <br>

        </div>

        <br>



    </div>

    </div>
</section>