<section id="inner-headline" style="padding-top: 90px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Services</h2>
            </div>
        </div>
    </div>
</section>
<section id="content" style="min-height: 600px;">
    <div class="container">
        <hr class="margin-bottom-50">
        <?php
        $i = 0;
        foreach ($activities as $row){
            $title = $row["title"];
            $description = $row['feed'];
            $icon = $row['icon'];
            $remainder = $i % 2;
            if($remainder == 0){
                echo "<div class='row'>";
            }
            ?>
            <div class="col-sm-6 info-blocks">
                <i class="icon-info-blocks  <?php echo $icon; ?>"></i>
                <div class="info-blocks-in">
                    <h3><?php echo $title; ?></h3>
                    <p><?php echo $description; ?></p>
                </div>
            </div>

            <?php
            if($remainder != 0){
                echo "</div>";
            }
            $i++;
        }
        ?>
    </div>
    </div>
</section>