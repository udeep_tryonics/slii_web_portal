<div id="wrapper">
    <!-- end header -->
    <section id="featured">
        <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                <?php
                foreach ($slider_images as $row){
                    $imageThumbURL = base_url().'assets/default/img/front/slides/'.$row["file_name"];
                    $imageURL = base_url().'assets/default/img/front/slides/'.$row["file_name"];
                    ?>
                    <li>
                        <img src="<?php echo $imageThumbURL; ?>" alt="" />
                        <div class="flex-caption">
                            <div class="item_introtext">
                                <strong>Elegance</strong>
                                <p>Go through our website for more</p> </div>
                        </div>
                    </li>
                <?php }
                ?>
            </ul>
        </div>
        <!-- end slider -->
        <div class="testimonial-area">
            <div class="testimonial-solid">
                <div class="container">
                    <div class="testi-icon-area">
                        <div class="quote">
                            <i class="fa fa-microphone"></i>
                        </div>
                    </div>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
<!--                            <li data-target="#carousel-example-generic" data-slide-to="0" class="">-->
<!--                                <a href="#"></a>-->
<!--                            </li>-->
<!--                            <li data-target="#carousel-example-generic" data-slide-to="1" class="">-->
<!--                                <a href="#"></a>-->
<!--                            </li>-->
<!--                            <li data-target="#carousel-example-generic" data-slide-to="2" class="active">-->
<!--                                <a href="#"></a>-->
<!--                            </li>-->
<!--                            <li data-target="#carousel-example-generic" data-slide-to="3" class="">-->
<!--                                <a href="#"></a>-->
<!--                            </li>-->

                            <?php
                            $i = 0;
                            foreach ($news_feeds as $row){
                                $feed = $row["feed"];
                                $title = $row["title"];
                                if( $i == 0 ){
                                    $active_class = 'active';
                                }else{
                                    $active_class = '';
                                }
                                ?>
                                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?> " class="<?php echo $active_class; ?>">
                                    <a href="#"></a>
                                </li>
                                <?php $i++; }
                            ?>

                        </ol>
                        <div class="carousel-inner">
                            <?php
                                $i = 0;
                                foreach ($news_feeds as $row){
                                    $feed = $row["feed"];
                                    $title = $row["title"];
                                    if( $i == 0 ){
                                        $active_class = 'active';
                                    }else{
                                        $active_class = '';
                                    }
                                    ?>
                                    <div class="item <?php echo $active_class; ?> ">
                                        <p>
                                            <b> <?php echo $title; ?> </b>
                                        </p>

                                        <p> <?php echo $feed; ?> </p>
                                    </div>
                                <?php $i++; }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="callaction">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="aligncenter"><h1 class="aligncenter">Welcome To SLII</h1><span class="clear spacer_responsive_hide_mobile " style="height:13px;display:block;"></span>The Sri Lanka Insurance Institute is the only educational body for those in the local
                        insurance field. It was established in 1981 to enhance professionalism in the practice of
                        insurance in Sri Lanka and develop the necessary human resources in insurance and other
                        related financial services. Over the years the institute has been governed by various
                        councils and now comprises of 11 dynamic individuals from the field of insurance. The
                        institute was established to fulfill the need to create and autonomous educational body
                        devoted to enhancing the standards of the insurance field in Sri Lanka.</div>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="skill-home"> <div class="skill-home-solid clearfix">


                        <?php
                        $i = 0;
                        foreach ($activities as $row) {
                            $feed = $row['feed'];
                            $feed = (strlen($feed) > 75) ? substr($feed, 0, 75) . '... </br> <a class="btn btn-default btn-xs" href=" '.base_url('services').' ">View More</a>' : $feed;
                            echo '<div class="col-md-3 text-center">
                            <span class="icons c1"><i class="'.$row['icon'].'"></i></span> <div class="box-area">
                                <h3>'.$row['title_short'].'</h3> 
                                <p>'.$feed.'</p> 
                            </div>
                        </div>';

                        }
                        ?>


<!--                        <div class="col-md-3 text-center">-->
<!--                            <span class="icons c1"><i class="fa fa-trophy"></i></span> <div class="box-area">-->
<!--                                <h3>Development</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 text-center">-->
<!--                            <span class="icons c2"><i class="fa fa-picture-o"></i></span> <div class="box-area">-->
<!--                                <h3>Pro.Training</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 text-center">-->
<!--                            <span class="icons c3"><i class="fa fa-desktop"></i></span> <div class="box-area">-->
<!--                                <h3>COURSES</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 text-center">-->
<!--                            <span class="icons c4"><i class="fa fa-globe"></i></span> <div class="box-area">-->
<!--                                <h3>Exsams</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>-->
<!--                            </div>-->
<!--                        </div>-->

                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Projects -->
        <section class="lates-work-wrap">

        <div id="particle"></div>
        <div id="overlay">   
            <div class="container">           
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="heading">Recent Works</h4>
                        <div class="row">
                            <section id="projects">
                                <ul id="thumbs" class="portfolio">

                                    <?php
                                    foreach ($recent_works as $row){
                                        $imageThumbURL = base_url().'assets/default/img/front/works/'.$row["file_name"];
                                        $imageURL = base_url().'assets/default/img/front/works/'.$row["file_name"];
                                        $title = $row["title"];
                                        ?>
                                        <li class="col-lg-4 design" data-id="id-0" data-type="web">
                                            <div class="item-thumbs">
                                                <img src="<?php echo $imageURL; ?>" alt=""><br>
                                                <p><?php echo $title; ?></p>
                                            </div>
                                        </li>
                                    <?php }
                                    ?>
                                </ul>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        </section>
    </section>
    <section class="se-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Modern Business Features</h2>
                    <p>The Modern Business template by Start Bootstrap includes:</p>
                    <ul>
                        <li>
                            <strong>Bootstrap v4</strong>
                        </li>
                        <li>jQuery</li>
                        <li>Font Awesome</li>
                        <li>Working contact form with validation</li>
                        <li>Unstyled page elements for easy customization</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
                </div>
                <div class="col-lg-6">
                    <iframe title="YouTube video player" class="youtube-player" type="text/html" width="640" height="390" src="<?php echo $videos[0]['video_url']; ?>" frameborder="0" allowFullScreen></iframe>
                </div>
            </div>
        </div>
    </section>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>
<script type="text/javascript">
    var options = {"particles":{"number":{"value":99,"density":{"enable":true,"value_area":552.4033491425909}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":3},"image":{"src":"img/github.svg","width":70,"height":100}},"opacity":{"value":1,"random":true,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":2,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":false,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":1.5782952832645452,"direction":"none","random":true,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"onclick":{"enable":true,"mode":"repulse"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":3},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":false};
</script>






