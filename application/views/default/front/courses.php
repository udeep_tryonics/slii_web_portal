<section id="inner-headline" style="padding-top: 90px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="pageTitle">Courses</h2>
			</div>
		</div>
	</div>
	</section>
	<section id="content" style="min-height: 600px;">
		<div class="container">	 
		<!-- end divider -->
		<div class="row"> 
			<div class="col-lg-3">
				<div class="pricing-box-item">
					<div class="pricing-heading">
						<h3><strong>INSURANCE FOUNDATION CERTIFICATE</strong></h3>
					</div>
					<div class="pricing-terms">
						<p class="title-row">FOR WHOM</p>
						<h6>BEGINERS</h6>
					</div>
					<div class="pricing-container">
						<ul>
							<li><i class="icon-ok"></i> 
								<p class="title-row">SUBJECTS</p>
								INTRODUCTION TO INSURANCE 
								<br>MARKETING IN INSUARNCE
								<br>MOTOR INSURANCE
								<br>FIRE INSURANCE
								<br>REINSURANCE
								<br>SINSURANCE CLAIM
							    <br>TAKAFUL INSURANCE
						        <br>REUGULATIONS IN THE INSURANCE INDUSTRY</li>
						    <li><i class="icon-ok"></i><p class="title-row">MEDIUM</p> ENGLISH/SINHALA</li>
							<li><i class="icon-ok"></i><p class="title-row">COMMENCEMENT</p> JANUARY/JULY</li>
							<li><i class="icon-ok"></i><p class="title-row">ENTRY QUALIFICATIONS</p> G.C.E (O/L)</li>
					
						</ul>
					</div>
					<div class="pricing-action">
						<!-- <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Contact Now</a> -->
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="pricing-box-item">
					<div class="pricing-heading">
						<h3><strong> DIPLOMA IN INSURANCE </strong></h3>
						<br>
					</div>
					<div class="pricing-terms">
						<p class="title-row">FOR WHOM</p>
						<h6>A TECHNICAL QUALIFICATION FOR INSURANCE STAFF</h6>
					</div>
					<div class="pricing-container">
						<ul>
							<li><i class="icon-ok"></i> 
								<p class="title-row">SUBJECTS</p>
								INSURANCE LAW
								<br>PERSONAL INSURANCES
								<br>UNDERWRITING PRACTICE
								<br>ADVANCED CLAIMS
								<br>LONG TERM INSURANCE BUSINESS
								<br>INSURANCE BUSINESS AND FINANCE
							    <br>INSURANCE REGULATORY PRACTICES
						       </li>
						    <li><i class="icon-ok"></i><p class="title-row">MEDIUM</p> ENGLISH</li>
							<li><i class="icon-ok"></i><p class="title-row">COMMENCEMENT</p> JANUARY/JULY</li>
							<li><i class="icon-ok"></i><p class="title-row">ENTRY QUALIFICATIONS</p> G.C.E.(A/L) OR MINIMUM TWO YEARS EXPERIENCE IN THE INDUSTRY</li>
					
						</ul>
					</div>
					<div class="pricing-action">
						<!-- <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Contact Now</a> -->
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="pricing-box-item">
					<div class="pricing-heading">
						<h3><strong>CERTIFICATE IN INSURANCE (“CERT-CII)</strong></h3>
					</div>
					<div class="pricing-terms">
						<p class="title-row">FOR WHOM</p>
						<h6>A CORE  QUALIFICATION  FOR INSURANCE STAFF IN THE INDUSTRY</h6>
					</div>
					<div class="pricing-container">
						<ul>
							<li><i class="icon-ok"></i> 
								<p class="title-row">SUBJECTS</p>
								AWARD IN GENERAL INSURANCE<br>
								<br>INSURANCE CLAIMS HANDLING PROCESS
								<br>
								<br>INSURANCE UNDERWRITING PROCES
								</li>
						    <li><i class="icon-ok"></i><p class="title-row">MEDIUM</p> ENGLISH</li>
							<li><i class="icon-ok"></i><p class="title-row">COMMENCEMENT</p> JANUARY/JUNE</li>
							<li><i class="icon-ok"></i><p class="title-row">ENTRY QUALIFICATIONS</p> G.C.E (O/L)</li>
					
						</ul>
					</div>
					<div class="pricing-action">
						<!-- <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Contact Now</a> -->
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="pricing-box-item">
					<div class="pricing-heading">
						<h3><strong>CERTIFICATE IN INSURANCE MARKETING</strong></h3>
					</div>
					<div class="pricing-terms">
						<p class="title-row">FOR WHOM</p>
						<h6>SALES AND MARKETING STAFF</h6>
					</div>
					<div class="pricing-container">
						<ul>
							<li><i class="icon-ok"></i> 
								<p class="title-row">SUBJECTS</p>
								FUNDAMENTALS OF SALES AND MARKETING
								<br>
								<br>STAKEHOLDER MANAGEMENT
								<br>
								<br>PRINCIPLES OF SALES AND MARKETING MANAGEMENT
								<br>
								<br>BASIC PRINCIPLES OF INSURANCE
								</li>
						    <li><i class="icon-ok"></i><p class="title-row">MEDIUM</p> ENGLISH</li>
							<li><i class="icon-ok"></i><p class="title-row">COMMENCEMENT</p> OCTOBER</li>
							<li><i class="icon-ok"></i><p class="title-row">ENTRY QUALIFICATIONS</p> G.C.E (O/L)</li>
					
						</ul>
					</div>
					<div class="pricing-action">
						<!-- <a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Contact Now</a> -->
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="pricing-box-item">
					<div class="pricing-heading">
						<h3><strong>CERTIFICATE COURSE IN INSURANCE MOTOR CLAIM ASSESMENT</strong></h3>
					</div>
					<div class="pricing-terms">
						<p class="title-row">FOR WHOM</p>
						<h6>MOTOR CLAIM HANDLERS </h6>
					</div>
					<div class="pricing-container">
						<ul>
							<li><i class="icon-ok"></i> 
								<p class="title-row">SUBJECTS</p>
								MOTOR POLICY								
								<br>ROLE OF A CLIAMS ADJUSTER							
								<br>LAWS APPLICABLE								
								<br>TECHNICAL REPORTS
								<br>ACCIDENT RECONSTRUCTION AND MANY OTHERS
								</li>
						    <li><i class="icon-ok"></i><p class="title-row">MEDIUM</p> ENGLISH</li>
							<li><i class="icon-ok"></i><p class="title-row">COMMENCEMENT</p> JANUARY</li>
							<li><i class="icon-ok"></i><p class="title-row">ENTRY QUALIFICATIONS</p> G.C.E (O/L)</li>
					
						</ul>
					</div>
					<div class="pricing-action">
					<!-- 	<a href="#" class="btn btn-medium btn-theme"><i class="icon-bolt"></i> Contact Now</a> -->
					</div>
				</div>
			</div>
			
		</div>
	</div>
	</section>