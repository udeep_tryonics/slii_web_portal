<section id="inner-headline" style="padding-top: 90px ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">Contact Us</h2>
            </div>
        </div>
    </div>
</section>
<section id="content">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCcszTOPXuR9uw6gECrcOgSZiZJBLBPtwY&callback=initMap"></script>
                <div style="overflow:hidden;height:300px;width:100%;">
                    <div id="gmap_canvas" style="height:300px;width:100%;">
                    </div>
                    <style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
                    <a class="google-map-code" href="http://www.trivoo.net" id="get-map-data">trivoo</a></div>

                <script type="text/javascript">
                    function init_map(){
                        var myOptions = {zoom:14,center:new google.maps.LatLng(6.913419,79.851359),mapTypeId: google.maps.MapTypeId.ROADMAP};
                        map = new google.maps.Map(document.getElementById("gmap_canvas"), myOptions);
                        marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(6.913419,79.851359)});
                        infowindow = new google.maps.InfoWindow({content:"<b>The Sri Lanka</b><br/>Insurance Institute<br/>" });
                        google.maps.event.addListener(marker, "click", function(){infowindow.open(map,marker);});
                        infowindow.open(map,marker);}google.maps.event.addDomListener(window, 'load', init_map);
                </script>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <br>
                <div id="contactSuccess">
                </div>
                <div id="contactError">
                </div>
                <div class="contact-form">

                        <div class="form-group has-feedback">
                            <label for="name">Name*</label>
                            <input type="text" class="form-control contact" id="name" name="name" placeholder="">
                            <i class="fa fa-user form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="email">Email*</label>
                            <input type="email" class="form-control contact" id="email" name="email" placeholder="">
                            <i class="fa fa-envelope form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="subject">Subject*</label>
                            <input type="text" class="form-control contact" id="subject" name="subject" placeholder="">
                            <i class="fa fa-navicon form-control-feedback"></i>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="message">Message*</label>
                            <textarea class="form-control contact" rows="6" id="message" name="message" placeholder=""></textarea>
                            <i class="fa fa-pencil form-control-feedback"></i>
                        </div>
                        <button id="add_contat_details"  class="btn btn-default">Submit</button>

                </div>
            </div>
            <div class="col-md-6">

                <div class="span4"><div class="title-box clearfix "><h3 class="title-box_primary">Contact info</h3></div>
                    <!-- <h5>Lorem ipsum dolor sit amet, cadipisicing sit amet, consectetur adipisicing elit. Atque sed, quidem quis praesentium.</h5>
                    <p>Lorem ipsum dolor sit amet, cadipisicing sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, cadipisicing sit amet, consectetur adipisicing elit. Atque sed, quidem quis praesentium Atque sed, quidem quis praesentium, ut unde fuga error commodi architecto, laudantium culpa tenetur at id, beatae pet.<br>
                    </p> -->
                    <address>
                        <strong>The Company Name.<br>
                            12345 St John Point,<br>
                            Brisbean, ABC 12 St 11.</strong><br>
                        Telephone: +1 234 567 890<br>
                        FAX: +1 234 567 890<br>
                        E-mail: <a href="mailto:info@sitename.org">mail@sitename.org</a><br>
                    </address> </div>
            </div>
        </div>
    </div>

</section>