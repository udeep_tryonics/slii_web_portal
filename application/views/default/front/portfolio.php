<section id="inner-headline" style="padding-top: 90px ">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="pageTitle">PORTFOLIO</h2>
                <div id="back_to_albums" class="pull-right back_to_albums">
                </div>
            </div>
        </div>
    </div>
</section>
<section id="content" style="background: #141844; min-height: 600px;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="clearfix">
                </div>
                <div class="row">
                    <section id="projects">
                        <ul id="thumbs" class="portfolio">

                            <?php
                            foreach ($albums as $row){
                                $imageThumbURL = base_url().'assets/default/img/front/albums/'.$row["album_image"];
                                $imageURL = base_url().'assets/default/img/front/albums/'.$row["album_image"];
                                $title = $row["album_name"];
                                $id = $row['id'];
                                ?>

                                <li class="item-thumbs col-lg-3 design" data-id="id-0" data-type="web">
                                    <p style="color: white;"><u><?php echo $title;?></u></p>
                                    <a onclick="album_clk(<?php echo $id ?>)" class="hover-wrap fancybox2" data-fancybox-group="gallery" title="<?php echo $title; ?>" href="#">
                                        <span class="overlay-img"></span>
                                        <span class="overlay-img-thumb"><i class="icon-info-blocks fa fa-code"></i></span>
                                    </a>
                                    <img src="<?php echo $imageURL; ?>" alt="">
                                </li>
                            <?php }
                            ?>
                        </ul>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>