<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">

        <div><img class="img-responsive slii_logo" src="<?php echo base_url('assets/'.THEME).'app/images/logo.jpg';?>"></div>
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="<?php echo base_url('user/auth'); ?>" method="post">
            <div class="form-group has-feedback">
                <input type="text" name="username" class="form-control" placeholder="Username">
                <span class="glyphicon glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <?php
                        if(!empty($sp_message)){
                    ?>
                    <p class="text-red"><?php if(isset($sp_message['message'])){echo $sp_message['message'];} ?></p>
                    <?php } ?>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <!--<a href="#">I forgot my password</a><br>-->
        <div><small>Powered by </small><a href="http://www.tryonics.com"><img width="100" src="<?php echo base_url('assets/'.THEME).'app/images/Logo-300x76.png';?>"></a></div>
    </div>
    <!-- /.login-box-body -->
</div>
