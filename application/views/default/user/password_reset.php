<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Password Reset
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Password Reset</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box-body">
                <body class="hold-transition register-page">
                <div class="register-box">
                    <div class="register-logo">
                    </div>
                    <div class="register-box-body">
                        <p class="login-box-msg">Reset Your Password</p>
                        <form action="<?php echo base_url('user/auth/password_reset'); ?>" method="post">
                            <div class="form-group has-feedback">
                                <input type="password" name="password" class="form-control" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                            <div class="form-group has-feedback">
                                <input type="password" name="retype_password" class="form-control" placeholder="Retype password">
                                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                            </div>
                            <div class="row">
                                <div class="col-xs-7">
                                    <?php
                                    if(!empty($sp_message) &&  $sp_message['type'] == 'error' ){
                                        ?>
                                        <p class="text-red"><?php if(isset($sp_message['message'])){echo $sp_message['message'];} ?></p>
                                    <?php } ?>
                                    <?php
                                    if(!empty($sp_message) &&  $sp_message['type'] == 'success' ){
                                        ?>
                                        <p class="text-green"><?php if(isset($sp_message['message'])){echo $sp_message['message'];} ?></p>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-5">
                                    <button type="submit" class="btn btn-primary btn-block btn-flat">Password Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
    </section>
</div>
