<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Our Contact</h5>
                    <address>
                        <strong>Core company Inc</strong><br>
                        JC Main Road, Near Silnile tower<br>
                        Pin-21542 NewYork US.</address>
                    <p>
                        <i class="icon-phone"></i> (123) 456-789 - 1255-12584 <br>
                        <i class="icon-envelope-alt"></i> email@domainname.com
                    </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Quick Links</h5>
                    <ul class="link-list">
                        <li><a href="http://www.ibsl.lk/">IBSL</a></li>
                        <li><a href="http://www.sliba.lk/">SLIBA</a></li>
                        <li><a href="https://www.insuranceinstituteofindia.com/web/guest;jsessionid=905699B8F3AD0C00E73FA190FB658218">III</a></li>
                        <li><a href="https://www.slim.lk/">SLIM</a></li>
                        <li><a href="http://www.cii.co.uk/">CII</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Latest posts</h5>
                    <ul class="link-list">
                        <?php
                        foreach ($recent_works as $row){
                            $title = $row["title"];
                            ?>
                            <li><a href="#"><?php echo $title; ?></a></li>
                        <?php }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="widget">
                    <h5 class="widgetheading">Recent News</h5>
                    <ul class="link-list">
                        <?php
                        $i = 0;
                        foreach ($news_feeds as $row){
                            $feed = $row["feed"];
                            $title = $row["title"];
                            if( $i == 0 ){
                                $active_class = 'active';
                            }else{
                                $active_class = '';
                            }
                            ?>
                            <li><a href="#"><?php echo (strlen($feed) > 80) ? substr($feed, 0, 80) . '...' : $feed;?></a></li>
                            <?php $i++; }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="copyright">
                        <p>
                            <span>&copy; Core Site All right reserved. Template By </span><a href="http://webthemez.com" target="_blank">MikeUIUX</a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="social-network">
                        <li><a href="#" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" data-placement="top" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#" data-placement="top" title="Google plus"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
