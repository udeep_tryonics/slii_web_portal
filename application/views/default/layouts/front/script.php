<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.js"></script>
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/portfolio/jquery.quicksand.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/portfolio/setting.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.flexslider.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/animate.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/custom.js"></script>
<script>
    var CCApp = <?php echo json_encode(array('base_url'=>base_url()));?>;
</script>
<?php echo load_css_js('footer');?>