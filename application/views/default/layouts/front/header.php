<!-- start header -->
<header>
    <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/'.THEME);?>img/front/logo.png" alt="logo" width="90px" />  </a>
                Sri Lanka Insurance Institute
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">

                    <?php
                    $active_menu = $this->session->userdata('active_menu_front');
                    ?>

                    <?php
                    $active_class = $active_menu == 1 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url().">Home</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 2 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('about_us').">About Us</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 3 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('services').">Services</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 4 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('portfolio').">Portfolio</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 5 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('courses').">COURSES</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 6 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('contact').">Contact</a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 7 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('user/auth')."><span class=\"login-btz\"> Login </span> </a></li>";
                    ?>

                    <?php
                    $active_class = $active_menu == 8 ? 'active' : '';
                    echo "<li class=\"$active_class\"><a href=".base_url('member_register')."><span class=\"login-btz\"> Register</span> </a></li>";
                    ?>

                </ul>
            </div>
        </div>
    </div>
</header>