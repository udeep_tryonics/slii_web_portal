<head>
    <head>
        <meta charset="utf-8">
        <title>SLII</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="http://webthemez.com" />
        <!-- css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/fancybox/jquery.fancybox.css" rel="stylesheet">
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/jcarousel.css" rel="stylesheet" />
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/flexslider.css" rel="stylesheet" />
        <link href="<?php echo base_url('assets/'.THEME);?>js/front/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/style.css" rel="stylesheet" />

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

    </head>
</head>