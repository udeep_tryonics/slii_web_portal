<!-- start header -->
<header>
    <div class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html"><img src="<?php echo base_url('assets/'.THEME);?>img/front/logo.png" alt="logo" width="90px" /></a>
            </div>
            <div class="navbar-collapse collapse ">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="<?php echo base_url()?>">Home</a></li>
                    <li><a href="<?php echo base_url('about_us')?>">About Us</a></li>
                    <li><a href="<?php echo base_url('services')?>">Services</a></li>
                    <li><a href="<?php echo base_url('portfolio')?>">Portfolio</a></li>
                    <li><a href="<?php echo base_url('courses')?>">COURSES</a></li>
                    <li><a href="<?php echo base_url('contact')?>">Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</header>