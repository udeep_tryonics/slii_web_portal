<!DOCTYPE html>
<html>
<?php $this->load->view(THEME.'layouts/front_register/head'); ?>
<body>
<?php  $this->load->view(THEME.'layouts/front/header'); ?>
<?php if(isset($current_view)){$this->load->view($current_view);} ?>
<?php  $this->load->view(THEME.'layouts/front/footer'); ?>
<?php  $this->load->view(THEME.'layouts/front_register/script'); ?>
</body>
</html>