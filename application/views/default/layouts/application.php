<!DOCTYPE html>
<html>
<?php  $this->load->view(THEME.'layouts/common/head'); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <?php  $this->load->view(THEME.'layouts/common/header'); ?>
    <!-- Left side column. contains the logo and sidebar -->
    <?php  $this->load->view(THEME.'layouts/common/left-nav'); ?>
    <!-- Content Wrapper. Contains page content -->
    <?php if(isset($current_view)){$this->load->view($current_view);} ?>
    <!-- /.content-wrapper -->
    <?php  $this->load->view(THEME.'layouts/common/footer'); ?>
    <!-- Control Sidebar -->
    <?php  $this->load->view(THEME.'layouts/common/right-nav'); ?>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<?php  $this->load->view(THEME.'layouts/common/script'); ?>
</body>
</html>
