<header class="main-header">

<?php $userdata = $this->session->userdata('login_data'); ?>
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SLII</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>SLII</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <?php
//        if(in_array("agent", $roles)){
        ?>

<!--        --><?php //} ?>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <?php
                    $this->load->view(THEME.'notification/notification',Registry::get('top_notification'));
                ?>
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url('assets/'.THEME);?>app/images/default_icons/call_man.png" class="user-image bg-white" alt="User Image">
                        <span class="hidden-xs"><?php echo $userdata['username'];?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url('assets/'.THEME);?>dist/img/user_default.png" class="img-circle" alt="User Image">

                            <p>
                                <?php echo $userdata['username'];?>
<!--                                <small>Member since Nov. 2012</small>-->
                            </p>
                        </li>
                        <!-- Menu Body -->
<!--                        <li class="user-body">-->
<!--                            <div class="row">-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Followers</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Sales</a>-->
<!--                                </div>-->
<!--                                <div class="col-xs-4 text-center">-->
<!--                                    <a href="#">Friends</a>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </li>-->
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo base_url('user/auth/password_reset');?>" class="btn btn-default btn-flat">Password Change</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo base_url('user/auth/logout');?>" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
<!--                <li>-->
<!--                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>-->
<!--                </li>-->
            </ul>
        </div>

    </nav>
</header>