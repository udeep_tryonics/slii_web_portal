<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('assets/'.THEME);?>dist/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url('assets/'.THEME);?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('assets/'.THEME);?>js/Chart.min.js"></script>
<!-- Sweet Alert -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/sweet_alert/dist/sweetalert.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>app/js/common/app.js"></script>
<!-- Validation Engine -->
<script src="<?php echo base_url('assets/'.THEME);?>js/jquery.validationEngine.js">"></script>
<!-- Validation Engine -->
<script src="<?php echo base_url('assets/'.THEME);?>js/jquery.validationEngine-en.js">"></script>
<!-- Moment Js -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/moment/min/moment.min.js"></script>
<!-- Date Picker -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Jquery Time Picker -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/jquery-timepicker/jquery.timepicker.js"></script>
<!-- Date Time Picker -->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<!-- Input Mask -->
<script src="<?php echo base_url('assets/'.THEME);?>plugins/input-mask/jquery.inputmask.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/'.THEME);?>plugins/iCheck/icheck.min.js"></script>
<!--image gallery-->
<!-- fancybox JS library -->
<script src="<?php echo base_url('assets/'.THEME);?>js/jquery.fancybox.js"></script>
<!-- Custom File Input -->
<script src="<?php echo base_url('assets/'.THEME);?>js/custom-file-input.js"></script>
<!--Choosen-->
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/chosen_v1.8.3/chosen.jquery.min.js"></script>


<script>
    var CCApp = <?php echo json_encode(array('base_url'=>base_url()));?>;
</script>
<script src="<?php echo base_url('assets/'.THEME);?>app/js/common/notification.js">"></script>
<?php echo load_css_js('footer');?>
