<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2014-2017 <a href="http://www.tryonics.com/">Tryonics (Pvt) Ltd.</a></strong> All rights
    reserved.
</footer>