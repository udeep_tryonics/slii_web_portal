<?php $userdata = $this->session->userdata('login_data'); ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url('assets/'.THEME);?>app/images/default_icons/call_man.png" class="img-circle bg-white" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $userdata['username']?></p>
            </div>
        </div>
        <!-- search form -->
<!--        <form action="#" method="get" class="sidebar-form">-->
<!--            <div class="input-group">-->
<!--                <input type="text" name="q" class="form-control" placeholder="Search...">-->
<!--          <span class="input-group-btn">-->
<!--                <button type="submit" name="search" id="search-btn" class="btn btn-flat">-->
<!--                    <i class="fa fa-search"></i>-->
<!--                </button>-->
<!--              </span>-->
<!--            </div>-->
<!--        </form>-->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
<!--            <li class="header">MAIN NAVIGATION</li>-->
<!--            <li class="active treeview menu-open">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>-->
<!--                    <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-files-o"></i>-->
<!--                    <span>Layout Options</span>-->
<!--            <span class="pull-right-container">-->
<!--              <span class="label label-primary pull-right">4</span>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>-->
<!--                    <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>-->
<!--                    <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>-->
<!--                    <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>-->
<!--                </ul>-->
<!--            </li>-->

<!--            <li>-->
<!--                <a href=".base_url('admin/courses').">-->
<!--                    <i class=\"fa fa-info-circle\"></i> <span>Courses</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('admin/subjects').">-->
<!--                    <i class=\"fa fa-info-circle\"></i> <span>Subjects</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('admin/course_payment').">-->
<!--                    <i class=\"fa fa-info-circle\"></i> <span>Course Payments</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('admin/profile').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Profile</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('admin/user_mgt').">-->
<!--                    <i class=\"fa fa-users\"></i> <span>User Management</span>-->
<!--                </a>-->
<!--            </li>-->

            <?php
            $roles = $userdata['roles'];
            $active_menu = $this->session->userdata('active_menu');
            if( in_array("slii_admin", $roles) ){
                ?>

                <?php
                $active_class = $active_menu == 8 ? 'active' : '';
                echo "<li class='$active_class'>
                        <a href=".base_url('admin/profile').">
                            <i class=\"fa far fa-image\"></i> <span>Profile</span>
                        </a>
                    </li>";
                ?>

                <?php
                $active_class = $active_menu == 1 ? 'active' : '';
                echo "<li class='$active_class'>
                        <a href=".base_url('admin/change_sliders').">
                            <i class=\"fa far fa-image\"></i> <span>Change Sliders</span>
                        </a>
                    </li>";
                ?>

                <?php
                $active_class = $active_menu == 2 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/add_gallery_images').">
                        <i class=\"fa far fa-image\"></i> <span>Photo Gallery</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 9 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/albums').">
                        <i class=\"fa far fa-image\"></i> <span>Albums</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 3 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/add_news_feeds').">
                        <i class=\"fa fa-info-circle\"></i> <span>News Feeds</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 4 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/videos').">
                        <i class=\"fa fa-youtube\"></i> <span>Videos</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 5 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/recent_works').">
                        <i class=\"fa fa-briefcase\"></i> <span>Recent Works</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 6 ? 'active' : '';
                echo "<li class='$active_class'>
                    <a href=".base_url('admin/activities').">
                        <i class=\"fa fa-info-circle\"></i> <span>Activities</span>
                    </a>
                </li>";
                ?>

                <?php
                $active_class = $active_menu == 7 ? 'active' : '';
                echo "<li class='$active_class'>
                <a href=".base_url('admin/messages').">
                    <i class=\"fa fa-envelope\"></i> <span>Messages</span>
                </a>
                </li>";
                ?>

                <?php
            }
            ?>

<!--            elseif(in_array("insurance_company", $roles)){-->
<!--            echo "<li>-->
<!--                <a href=".base_url('ins_comp/profile').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Profile</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('ins_comp/add_members').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Add Member</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('ins_comp/members').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Members</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            ";-->
<!--            }-->
<!--            elseif(in_array("slii_member", $roles)){-->
<!--            echo "<li>-->
<!--                <a href=".base_url('member/profile').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Profile</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('member/apply_for_courses').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Apply for courses</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href=".base_url('member/exams').">-->
<!--                    <i class=\"fa fa-user\"></i> <span>Exams</span>-->
<!--                </a>-->
<!--            </li>-->
<!--            ";-->
<!--            }else{-->
<!--            echo "No Privilages Set";-->
<!--            }-->
<!--            ?>-->






<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-pie-chart"></i>-->
<!--                    <span>Charts</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/charts/chartjs.html"><i class="fa fa-circle-o"></i> ChartJS</a></li>-->
<!--                    <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Morris</a></li>-->
<!--                    <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Flot</a></li>-->
<!--                    <li><a href="pages/charts/inline.html"><i class="fa fa-circle-o"></i> Inline charts</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-laptop"></i>-->
<!--                    <span>UI Elements</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/UI/general.html"><i class="fa fa-circle-o"></i> General</a></li>-->
<!--                    <li><a href="pages/UI/icons.html"><i class="fa fa-circle-o"></i> Icons</a></li>-->
<!--                    <li><a href="pages/UI/buttons.html"><i class="fa fa-circle-o"></i> Buttons</a></li>-->
<!--                    <li><a href="pages/UI/sliders.html"><i class="fa fa-circle-o"></i> Sliders</a></li>-->
<!--                    <li><a href="pages/UI/timeline.html"><i class="fa fa-circle-o"></i> Timeline</a></li>-->
<!--                    <li><a href="pages/UI/modals.html"><i class="fa fa-circle-o"></i> Modals</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-edit"></i> <span>Forms</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/forms/general.html"><i class="fa fa-circle-o"></i> General Elements</a></li>-->
<!--                    <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Advanced Elements</a></li>-->
<!--                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Editors</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-table"></i> <span>Tables</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>-->
<!--                    <li><a href="pages/tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="pages/calendar.html">-->
<!--                    <i class="fa fa-calendar"></i> <span>Calendar</span>-->
<!--            <span class="pull-right-container">-->
<!--              <small class="label pull-right bg-red">3</small>-->
<!--              <small class="label pull-right bg-blue">17</small>-->
<!--            </span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="pages/mailbox/mailbox.html">-->
<!--                    <i class="fa fa-envelope"></i> <span>Mailbox</span>-->
<!--            <span class="pull-right-container">-->
<!--              <small class="label pull-right bg-yellow">12</small>-->
<!--              <small class="label pull-right bg-green">16</small>-->
<!--              <small class="label pull-right bg-red">5</small>-->
<!--            </span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-folder"></i> <span>Examples</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="pages/examples/invoice.html"><i class="fa fa-circle-o"></i> Invoice</a></li>-->
<!--                    <li><a href="pages/examples/profile.html"><i class="fa fa-circle-o"></i> Profile</a></li>-->
<!--                    <li><a href="pages/examples/login.html"><i class="fa fa-circle-o"></i> Login</a></li>-->
<!--                    <li><a href="pages/examples/register.html"><i class="fa fa-circle-o"></i> Register</a></li>-->
<!--                    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-circle-o"></i> Lockscreen</a></li>-->
<!--                    <li><a href="pages/examples/404.html"><i class="fa fa-circle-o"></i> 404 Error</a></li>-->
<!--                    <li><a href="pages/examples/500.html"><i class="fa fa-circle-o"></i> 500 Error</a></li>-->
<!--                    <li><a href="pages/examples/blank.html"><i class="fa fa-circle-o"></i> Blank Page</a></li>-->
<!--                    <li><a href="pages/examples/pace.html"><i class="fa fa-circle-o"></i> Pace Page</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li class="treeview">-->
<!--                <a href="#">-->
<!--                    <i class="fa fa-share"></i> <span>Multilevel</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--                </a>-->
<!--                <ul class="treeview-menu">-->
<!--                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--                    <li class="treeview">-->
<!--                        <a href="#"><i class="fa fa-circle-o"></i> Level One-->
<!--                <span class="pull-right-container">-->
<!--                  <i class="fa fa-angle-left pull-right"></i>-->
<!--                </span>-->
<!--                        </a>-->
<!--                        <ul class="treeview-menu">-->
<!--                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>-->
<!--                            <li class="treeview">-->
<!--                                <a href="#"><i class="fa fa-circle-o"></i> Level Two-->
<!--                    <span class="pull-right-container">-->
<!--                      <i class="fa fa-angle-left pull-right"></i>-->
<!--                    </span>-->
<!--                                </a>-->
<!--                                <ul class="treeview-menu">-->
<!--                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>-->
<!--                                </ul>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>-->
<!--                </ul>-->
<!--            </li>-->
<!--            <li><a href="https://adminlte.io/docs"><i class="fa fa-book"></i> <span>Documentation</span></a></li>-->
<!--            <li class="header">LABELS</li>-->
<!--            <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li>-->
<!--            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>-->
<!--            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>