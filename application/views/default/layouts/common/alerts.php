<?php
$_message = array();
if($this->session->flashdata('sp_message')) {
    $_message = $this->session->flashdata('sp_message');
}elseif(isset($sp_message)){
    $_message = $sp_message;
}
if(!empty($_message)){
    if($_message['type'] == 'success'){
        $class = 'alert-success';
        $sharp = 'Done';
        $icon = "fa-check";
    }elseif($_message['type'] == 'warning'){
        $class = 'alert-warning';
        $sharp = 'Warning';
        $icon = "fa-warning";
    }
    else{
        $sharp = 'Oops';
        $class = 'alert-danger';
        $icon = 'fa-ban';
    }
?>
    <div class="alert <?php echo $class ?> alert-dismissible" id="cc-alerts">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h4><i class="icon fa <?php echo $icon ?>"></i> <?php echo $sharp ?>!</h4>
        <?php echo $_message['message']; ?>
    </div>
<?php
}
?>