<!DOCTYPE html>
<html>
<?php  $this->load->view(THEME.'layouts/common/head'); ?>
<body class="hold-transition login-page">
<div class="login-wrapper">
    <?php if(isset($current_view)){$this->load->view($current_view);} ?>
</div>
<?php  $this->load->view(THEME.'layouts/common/script'); ?>
<?php  $this->load->view(THEME.'layouts/common/login_script'); ?>
</body>
</html>