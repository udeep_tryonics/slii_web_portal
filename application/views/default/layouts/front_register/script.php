<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/bootstrap.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.fancybox.pack.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.fancybox-media.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/portfolio/jquery.quicksand.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/portfolio/setting.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/jquery.flexslider.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/animate.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>js/front/custom.js"></script>
<!-- <script src="<?php echo base_url('assets/'.THEME);?>js/front/owl-carousel/owl.carousel.js"></script> -->
<script src="<?php echo base_url('assets/'.THEME);?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/jquery-timepicker/jquery.timepicker.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url('assets/'.THEME);?>app/js/common/app.js"></script>
<script>
    var CCApp = <?php echo json_encode(array('base_url'=>base_url()));?>;
</script>
<?php echo load_css_js('footer');?>