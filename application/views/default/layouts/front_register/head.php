<head>
    <head>
        <meta charset="utf-8">
        <title>SLII</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="" />
        <meta name="author" content="http://webthemez.com" />
        <!-- css -->
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/fancybox/jquery.fancybox.css" rel="stylesheet">
        <!-- <link href="<?php echo base_url('assets/'.THEME);?>css/front/jcarousel.css" rel="stylesheet" /> -->
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/flexslider.css" rel="stylesheet" />
        <!-- <link href="<?php echo base_url('assets/'.THEME);?>js/front/owl-carousel/owl.carousel.css" rel="stylesheet"> -->
        <link href="<?php echo base_url('assets/'.THEME);?>css/front/style.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>bower_components/jquery-timepicker/jquery.timepicker.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>plugins/iCheck/all.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet"
              href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <link rel="stylesheet" href="<?php echo base_url('assets/'.THEME);?>app/css/style.css">
        <?php echo load_css_js('header');?>

    </head>
</head>