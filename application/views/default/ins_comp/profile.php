<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SLII Dashboard
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/'.THEME);?>app/images/default_icons/call_man.png" alt="User profile picture">

                        <h3 class="profile-username text-center"><?php echo $this->session->userdata('login_data')['username'] ?></h3>

                        <p class="text-muted text-center"><?php echo $this->session->userdata('login_data')['companyname'] ?></p>

<!--                        <ul class="list-group list-group-unbordered">-->
<!--                            <li class="list-group-item">-->
<!--                                <b>Followers</b> <a class="pull-right">1,322</a>-->
<!--                            </li>-->
<!--                            <li class="list-group-item">-->
<!--                                <b>Following</b> <a class="pull-right">543</a>-->
<!--                            </li>-->
<!--                            <li class="list-group-item">-->
<!--                                <b>Friends</b> <a class="pull-right">13,287</a>-->
<!--                            </li>-->
<!--                        </ul>-->

                        <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>

        </div>
    </section>
    <!-- /.content -->
</div>


