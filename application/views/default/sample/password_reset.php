<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
    </div>
    <div class="register-box-body">
        <p class="login-box-msg">Reset Your Password</p>
        <form action="" method="post">
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" placeholder="Retype password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-5">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Password Reset</button>
                </div>
            </div>
        </form>
    </div>
</div>
