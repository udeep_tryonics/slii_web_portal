<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Videos
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Videos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-lg-12">
                            <div class="col-lg-2">
                                <label>
                                    Video Url
                                </label>
                            </div>

                            <div class="col-lg-4">
                                <input type="text" class="form-control" id="video_url">
                            </div>

                            <div class="col-lg-6">
                                <button onclick="add_video()" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-lg-12">

                            <?php
                            foreach ($videos as $video){
                                $video_url = $video["video_url"];
                                ?>
                                <div class="img-gallery-frame">

                                    <div class="gallery-box-sm-12">
                                        <button class="btn btn-xs btn-danger" onclick="remove_video('<?php echo $video['id']  ?>')">Delete</button>
                                        <?php if ($video['status'] == '1') { ?>
                                            <button class="btn btn-xs btn-success" onclick="set_active_video('<?php echo $video['id']  ?>' , 0)">InActive</button>
                                        <?php } else { ?>
                                            <button class="btn btn-xs btn-primary" onclick="set_active_video('<?php echo $video['id']  ?>' , 1)">Active</button>
                                        <?php } ?>
                                    </div>
                                    <div>
                                        <iframe title="YouTube video player" class="youtube-player" type="text/html" width="240" height="190" src="<?php echo $video_url ?>" frameborder="0" allowFullScreen></iframe>
                                    </div>

                                    <div class="gallery-box-sm-12">
                                        <a class="white" href="<?php echo $video_url ?>"><?php echo $video_url ?></a>
                                    </div>

                                </div>

                            <?php }
                            ?>

                        </div>
                    </div>
                </div>

                </br>

            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


