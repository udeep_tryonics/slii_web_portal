<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             News Feeds
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">News Feeds</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-6">
                            <label>News Feed</label>
                            <textarea class="form-control" id="news_feed">
                            </textarea>
                        </div>
                        <div class="col-sm-6">
                            <label>News Title</label>
                            <input type="text" class="form-control" name="news_title" id="news_title"/>
                        </div>
                        <div class="col-sm-12">
                            </br>
                            <button class="btn btn-primary" id="add_news_feed" onclick="add_feed()">Add</button>
                            <input type="hidden" name="hidden_input_feed" id="hidden_input_feed"/>
                        </div>
                    </div>
                </div>

                </br>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-12 col-md-12">
                            <table id="news_feed_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="menuUnSelected" align="left">#</th>
                                <th class="menuUnSelected" align="left">News Feed</th>
                                <th class="menuUnSelected" align="left">Title</th>
                                <th class="menuUnSelected" align="left">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /.content -->
</div>