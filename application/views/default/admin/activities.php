<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Activities
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Activities</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>
        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="col-sm-4">
                                <label>Description</label>
                                    <textarea class="form-control" id="news_feed">
                                </textarea>
                            </div>
                            <div class="col-sm-4">
                                <label>Title</label>
                                <input type="text" class="form-control" name="news_title" id="news_title"/>
                            </div>
                            <div class="col-sm-2">
                                <label>Title Short</label>
                                <input type="text" class="form-control" name="news_title_short" id="news_title_short"/>
                            </div>
                            <div class="col-sm-2">
                                <label>Icon</label>
                                    <input type="text" class="form-control" name="icon" id="icon"/>
                                </textarea>
                            </div>
                            <div class="col-sm-12">
                                </br>
                                <button class="btn btn-primary" id="add_news_feed" onclick="add_activities()">Add</button>
                                <input type="hidden" name="hidden_input_feed" id="hidden_input_feed"/>
                            </div>
                        </div>
                    </div>

                    </br>

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="col-sm-12 col-md-12">
                                <table id="activities_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th align="left">#</th>
                                        <th align="left">Description</th>
                                        <th align="left">Title</th>
                                        <th align="left">Title Short</th>
                                        <th align="left">Icon</th>
                                        <th align="left">Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>