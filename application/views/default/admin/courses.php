<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Courses
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <label>Course Name</label>
                            <input type="text" id="course_name" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <label>Description</label>
                            <input type="text" id="course_code" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <label>Subjects</label>
                            <select id="course_subjects" class="form-control chzn-select" multiple="true">
                                <?php
                                    foreach ($subjects as $subject){
                                        echo '<option value='.$subject['id'].'>'.$subject['subject_name'].'</option>';
                                    }
                                ?>
                            </select>
                            <input type="hidden" id="course_id_hidden">
                        </div>
                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <button class="btn btn-success btn-sm" id="add-course" onclick="add_course()">Add</button>
                        </div>
                    </div>
                </div>

                </br>

                <div class="col-sm-12">
                    <table id="slii_courses_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="menuUnSelected" align="left">User Id</th>
                            <th class="menuUnSelected" align="left">Course Name</th>
                            <th class="menuUnSelected" align="left">Course Description</th>
                            <th class="menuUnSelected" align="left">Subjects</th>
                            <th class="menuUnSelected" align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


