<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Management
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="col-sm-12">
                    <table id="user_mgt" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="menuUnSelected" align="left">User Id</th>
                            <th class="menuUnSelected" align="left">User Name</th>
                            <th class="menuUnSelected" align="left">Role</th>
                            <th class="menuUnSelected" align="left">Company</th>
                            <th class="menuUnSelected" align="left">Is Active</th>
                            <th class="menuUnSelected" align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


