<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Recent Works
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Recent Works</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <form action="recent_works" method="post" enctype="multipart/form-data">
                            <div class="col-sm-7">
                                <input type="file" name="userfiles[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a file&hellip;</strong></label>

                            </div>
                            <div class="col-sm-2">
                                <input class="btn btn-primary btn-lg" type="submit" value="Upload Image" name="upload">
                            </div>
                        </form>
                    </div>

                    <div class="col-sm-12">
                        <div class="gallery">
                            <?php
                            foreach ($recent_works_images as $row){
                                $imageThumbURL = base_url().'assets/default/img/front/works/'.$row["file_name"];
                                $imageURL = base_url().'assets/default/img/front/works/'.$row["file_name"];
                                $title = $row["title"];

                                ?>
                                <div class="img-gallery-frame">
                                    <div class="gallery-box-sm-12">
                                        <input class="img-title" id="title<?php echo $row['id']; ?>" value="<?php echo $title; ?>" type="text"/>
                                    </div>
                                    <div class="gallery-box-sm-12">
                                        <button class="btn btn-xs btn-danger" onclick="remove_recent_works_img('<?php echo $row['id']  ?>')">Delete</button>
                                        <button class="btn btn-xs btn-success" onclick="update_recent_works_img('<?php echo $row['id']  ?>')">Update</button>
                                        <?php if ($row['status'] == '1') { ?>
                                            <button class="btn btn-xs btn-success" onclick="set_active_recent_works('<?php echo $row['id']  ?>' , 0)">InActive</button>
                                        <?php } else { ?>
                                            <button class="btn btn-xs btn-primary" onclick="set_active_recent_works('<?php echo $row['id']  ?>' , 1)">Active</button>
                                        <?php } ?>
                                    </div>
                                    <div>
                                        <a href="<?php echo $imageURL; ?>" data-fancybox="group" data-caption="<?php echo $row["title"]; ?>" >
                                            <img class="gallery-imgs" src="<?php echo $imageThumbURL; ?>" alt="" />
                                            <div class="gallery-title"><?php echo (strlen($title) > 100) ? substr($title, 0, 100) . '...' : $title;?></div>
                                        </a>
                                    </div>
                                    <div class="gallery-box-sm-12">

                                    </div>
                                </div>

                            <?php }
                            ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


