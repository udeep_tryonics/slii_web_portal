<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Albums
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Albums</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-sm-12">
                        <form action="albums" method="post" enctype="multipart/form-data">
                            <div class="col-sm-7">
                                <input type="file" name="userfiles[]" id="file-7" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" multiple />
                                <label for="file-7"><span></span> <strong><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> Choose a file&hellip;</strong></label>
                            </div>

                            <div class="col-sm-3">
                                <label>
                                    Album Name
                                </label>
                                <input class="form-control" name="album_name"/>
                            </div>

                            <div class="col-sm-2">
                                <label>
                                    Upload Here..
                                </label>
                                <input class="btn btn-primary btn-sm" type="submit" value="Upload Image" name="upload">
                            </div>
                        </form>
                    </div>

                    <div class="col-md-12">
                        <table id="albums_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="menuUnSelected" align="left">Album Id</th>
                                <th class="menuUnSelected" align="left">Album Name</th>
                                <th class="menuUnSelected" align="left">Album Image</th>
                                <th class="menuUnSelected" align="left">Status</th>
                                <th class="menuUnSelected" align="left">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>