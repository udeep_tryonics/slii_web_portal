<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Course Payment
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Course Payment</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="col-sm-12">
                    <table id="course_payment_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="menuUnSelected" align="left">User Id</th>
                            <th class="menuUnSelected" align="left">User Name</th>
                            <th class="menuUnSelected" align="left">Course</th>
                            <th class="menuUnSelected" align="left">Course Fee</th>
                            <th class="menuUnSelected" align="left">Outstanding</th>
                            <th class="menuUnSelected" align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="course_payment_model" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <td>Amount</td>
                                </tr>
                            </thead>
                            <tbody id="course_payment_details">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </section>

</div>


