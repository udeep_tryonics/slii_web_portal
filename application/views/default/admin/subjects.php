<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Subjects
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <label>Subject Name</label>
                            <input type="text" id="subject_name" class="form-control">
                        </div>

                        <div class="col-sm-4">
                            <label>Description</label>
                            <input type="text" id="subject_code" class="form-control">
                        </div>

                    </div>
                </div>

                </br>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <button class="btn btn-success btn-sm" onclick="add_subject()">Add</button>
                        </div>
                    </div>
                </div>

                </br>

                <div class="col-sm-12">
                    <table id="slii_subjects_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="menuUnSelected" align="left">User Id</th>
                            <th class="menuUnSelected" align="left">Subject Name</th>
                            <th class="menuUnSelected" align="left">Subject Description</th>
                            <th class="menuUnSelected" align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


