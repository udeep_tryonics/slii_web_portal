<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Services
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Services</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>

        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <label>Service Title</label>
                            <input type="text" id="title" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <label>Service Description</label>
                            <input type="text" id="description" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <label>Icon</label>
                            <input type="text" id="icon" class="form-control">
                        </div>
                    </div>
                </div>
                </br>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <button class="btn btn-success btn-sm" id="add-service" onclick="add_service()">Add</button>
                        </div>
                    </div>
                </div>

                </br>

                <div class="col-sm-12">
                    <table id="slii_services_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th class="menuUnSelected" align="left">Id</th>
                            <th class="menuUnSelected" align="left">Service Title</th>
                            <th class="menuUnSelected" align="left">Service Description</th>
                            <th class="menuUnSelected" align="left">Icon</th>
                            <th class="menuUnSelected" align="left">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>


