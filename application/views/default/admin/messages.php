<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Messages
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Messages</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>
        <div class="box">
            <div class="box-header">
            </div>
            <div class="box-body">
                <div class="row">

                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="col-sm-12 col-md-12">
                                <table id="msg_tbl" class="table table-hover call_list_cls table-bordered table-striped" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th align="left">#</th>
                                        <th align="left">Name</th>
                                        <th align="left">Email</th>
                                        <th align="left">Subject</th>
                                        <th align="left">Message</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>