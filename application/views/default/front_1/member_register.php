<div class="wrapper">
    <!-- Main content -->
    <section class="content">
        <?php $this->load->view(THEME.'layouts/common/alerts');?>
            <div class="add-member-form-front">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="surname">Personal Information</label>
                                </br>
                                Mr
                                <input type="radio"  class="form-check-input minimal" name="surname[]" value="mr" <?php echo in_array("mr" ,$member_data['surname']) ? 'checked' : '' ?> >
                                Mrs
                                <input type="radio" class="form-check-input minimal" name="surname[]" value="mrs" <?php echo in_array("mrs" ,$member_data['surname']) ? 'checked' : '' ?>>
                                Ms
                                <input type="radio" class="form-check-input minimal" name="surname[]" value="ms" <?php echo in_array("ms" ,$member_data['surname']) ? 'checked' : '' ?>>
                                Dr
                                <input type="radio" class="form-check-input minimal" name="surname[]" value="dr" <?php echo in_array("dr" ,$member_data['surname']) ? 'checked' : '' ?>>
                                Other
                                <input type="radio" class="form-check-input minimal" name="surname[]" value="other" <?php echo in_array("other" ,$member_data['surname']) ? 'checked' : '' ?>>
                                <input type="hidden" id="slii_member_edit_id" value="<?php echo isset($member_data['slii_member_edit_id']) ? $member_data['slii_member_edit_id'] : '' ?>"/>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="fullname">Name in Full</label>
                                <input type="text" class="form-control" value="<?php echo isset($member_data['fullname']) ? $member_data['fullname'] : '' ?>" id="fullname" placeholder="Full Name">
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="dob">Date Of Birth</label>
                                <input type="text" class="form-control datepicker" value="<?php echo isset($member_data['dob']) ? $member_data['dob'] : '' ?>" id="dob" placeholder="Date Of Birth">
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">NIC</label>
                                <input type="text" class="form-control" value="<?php echo isset($member_data['nic']) ? $member_data['nic'] : '' ?>" id="nic" placeholder="NIC">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="surname">Payment Type</label>
                                </br>
                                Company
                                <input type="radio" class="form-check-input minimal" name="payment_type" value="1" <?php echo isset($member_data['payment_type']) && $member_data['payment_type'] == 1 ? 'checked' : '' ?> >
                                Individual
                                <input type="radio" class="form-check-input minimal" name="payment_type" value="2" <?php echo isset($member_data['payment_type']) && $member_data['payment_type'] == 2  ? 'checked' : '' ?>>
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3 ins-cmp-div">
                            <div class="form-group">
                                <label for="surname">Company</label>
                                <select class="form-control" id="company_payment" name="company_payment">
                                    <option disabled="disabled" selected="selected">--Select--</option>
                                    <?php
                                        foreach ( $ins_cmp_data as $ins_cmp ){
                                            echo '<option value='.$ins_cmp['cmp_id'].'>'.$ins_cmp['cmp_name'].'</option>';
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label>Prefered Mailing Adress</label>
                                </br>
                                <input type="radio" class="form-check-input minimal" name="pref_mail_adr[]" value='1' id="pref_mail_adr_home" <?php echo in_array("1" ,$member_data['pref_mail_adr']) ? 'checked' : '' ?>>
                                Home
                                <input type="radio" class="form-check-input minimal" name="pref_mail_adr[]" value='2' id="pref_mail_adr_company" <?php echo in_array("2" ,$member_data['pref_mail_adr']) ? 'checked' : '' ?>>
                                Company
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="cmp_name_adr">Company Name And Adress</label>
                                <input type="text" class="form-control" id="cmp_name_adr" value="<?php echo isset($member_data['cmp_name_adr']) ? $member_data['cmp_name_adr'] : '' ?>" placeholder="Company Name And Adress">
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="cmp_phone">Company Phone Number</label>
                                <input type="text" class="form-control" id="cmp_phone" value="<?php echo isset($member_data['cmp_phone']) ? $member_data['cmp_phone'] : '' ?>" placeholder="Company Phone Number" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="cmp_email">Company Email Adress</label>
                                <input type="text" class="form-control" id="cmp_email" value="<?php echo isset($member_data['cmp_email']) ? $member_data['cmp_email'] : '' ?>" placeholder="Company Email Adress">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="home_adr">Home Adress</label>
                                <input type="text" class="form-control"  value="<?php echo isset($member_data['home_adr']) ? $member_data['home_adr'] : '' ?>" id="home_adr" placeholder="Home Adress">
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="home_phone">Home Phone Number</label>
                                <input type="text" class="form-control" id="home_phone"  value="<?php echo isset($member_data['home_phone']) ? $member_data['home_phone'] : '' ?>" placeholder="Home Phone Number" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="home_mobile">Mobile Phone Number</label>
                                <input type="text" class="form-control" id="home_mobile"  value="<?php echo isset($member_data['home_mobile']) ? $member_data['home_mobile'] : '' ?>" placeholder="Mobile Phone Number" data-inputmask='"mask": "(999) 999-9999"' data-mask>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="home_email">Home Email Adress</label>
                                <input type="text" class="form-control" id="home_email"  value="<?php echo isset($member_data['home_email']) ? $member_data['home_email'] : '' ?>" placeholder="Home Email Adress">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="present_occupation">Present Occupation</label>
                                <input type="text" class="form-control" id="present_occupation" value="<?php echo isset($member_data['present_occupation']) ? $member_data['present_occupation'] : '' ?>" placeholder="Present Occupation">
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">Nature Of Current Industry</label>
                                </br>
                                <input type="radio" class="form-check-input minimal" name="current_industry_nature[]" value='1' id="current_industry_nature_home" <?php echo in_array("1" ,$member_data['current_industry_nature']) ? 'checked' : '' ?> >
                                Insurance
                                <input type="radio" class="form-check-input minimal" name="current_industry_nature[]" value='2' id="current_industry_nature_company" <?php echo in_array("2" ,$member_data['current_industry_nature']) ? 'checked' : '' ?> >
                                Non Insurance
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">If Insurance Please Specify</label>
                                </br>
                                <input type="radio" class="form-check-input minimal" name="insurance_type[]" value='1' id="insurance_type_home" <?php echo in_array("1" ,$member_data['insurance_type']) ? 'checked' : '' ?> >
                                Life
                                <input type="radio" class="form-check-input minimal" name="insurance_type[]" value='2' id="insurance_type_company" <?php echo in_array("2" ,$member_data['insurance_type']) ? 'checked' : '' ?> >
                                Non Life
                                <input type="radio" class="form-check-input minimal" name="insurance_type[]" value='3' id="insurance_type_company" <?php echo in_array("3" ,$member_data['insurance_type']) ? 'checked' : '' ?> >
                                Other
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label>Areas</label>
                                </br>
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="1" id="ar_underwriting" <?php echo in_array("1" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Underwriting
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="2" id="ar_claim_management" <?php echo in_array("2" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Claim Management
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="3" id="ar_re_insurance" <?php echo in_array("3" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Re Insurance
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="4" id="ar_risk_management" <?php echo in_array("4" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Risk Management
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="5" id="ar_marketing_sales" <?php echo in_array("5" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Marketing/Sales
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="6" id="ar_accounting_finance" <?php echo in_array("6" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Accounting Finance
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="7" id="ar_legal" <?php echo in_array("7" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Legal
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="8" id="ar_human_resources" <?php echo in_array("8" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Human Resources
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="9" id="ar_administration" <?php echo in_array("9" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Administration
                                <input type="checkbox" class="form-check-input minimal" name="ins_areas[]" value="10" id="ar_others" <?php echo in_array("10" ,$member_data['ins_areas']) ? 'checked' : '' ?>>
                                Other
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <label for="nic">Educatnal Quallification</label>
                                </br>
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="1" id="edu_qaulification_ol" <?php echo in_array("1" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                GCE/OL
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="2" id="edu_qaulification_al" <?php echo in_array("2" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                GCE/AL
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="3" id="edu_qaulification_acii" <?php echo in_array("3" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                ACII
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="4" id="edu_qaulification_fcii" <?php echo in_array("4" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                FCII
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="5" id="edu_qaulification_degree" <?php echo in_array("5" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                DEGREE
                                <input type="checkbox" class="form-check-input minimal" name="edu_qualifications[]" value="6" id="edu_qaulification_others" <?php echo in_array("6" ,$member_data['edu_qualifications']) ? 'checked' : '' ?>>
                                OTHERS
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">

                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">Name Of Company</label>
                                <input type="text" class="form-control" id="exp_name_of_company" placeholder="Name Of Company">
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">Job Title</label>
                                <input type="text" class="form-control" id="exp_job_title" placeholder="Job Title">
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">From</label>
                                <input type="text" class="form-control" id="exp_from" placeholder="Experience From">
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">To</label>
                                <input type="text" class="form-control" id="exp_to" placeholder="Experience To">
                            </div>
                        </div>

                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <label for="nic">Description Of Job</label>
                                <input type="text" class="form-control" id="exp_description" placeholder="Description Of Job">
                            </div>
                        </div>

                    </div>

                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group">
                                <button type="button" class="btn btn-info btn-xs" onclick="add_experience()">Add</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-6 col-md-6">
                            <table class="table table-bordered table-stripped">
                                <thead>
                                <th>Name Of Company</th>
                                <th>Title</th>
                                <th>From</th>
                                <th>To</th>
                                <th>Action</th>
                                </thead>
                                <tbody id="exp_tbl_body">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="col-sm-12 col-md-12">
                            <div class="form-group">
                                <button onclick="submit_member_data()" class="btn btn-primary btn-xs btn-submit pull-right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</div>
