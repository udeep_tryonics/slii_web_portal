<div id="wrapper">
    <!-- end header -->
    <section id="featured">
        <!-- Slider -->
        <div id="main-slider" class="flexslider">
            <ul class="slides">
                <?php
                foreach ($slider_images as $row){
                    $imageThumbURL = base_url().'assets/default/img/front/slides/'.$row["file_name"];
                    $imageURL = base_url().'assets/default/img/front/slides/'.$row["file_name"];
                    ?>
                    <li>
                        <img src="<?php echo $imageThumbURL; ?>" alt="" />
                        <div class="flex-caption">
                            <div class="item_introtext">
                                <strong>Elegance</strong>
                                <p>Go through our website for more</p> </div>
                        </div>
                    </li>
                <?php }
                ?>
            </ul>
        </div>
        <!-- end slider -->
        <div class="testimonial-area">
            <div class="testimonial-solid">
                <div class="container">
                    <div class="testi-icon-area">
                        <div class="quote">
                            <i class="fa fa-microphone"></i>
                        </div>
                    </div>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="">
                                <a href="#"></a>
                            </li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class="">
                                <a href="#"></a>
                            </li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class="active">
                                <a href="#"></a>
                            </li>
                            <li data-target="#carousel-example-generic" data-slide-to="3" class="">
                                <a href="#"></a>
                            </li>
                        </ol>
                        <div class="carousel-inner">
                            <?php
                            $i = 0;
                            foreach ($news_feeds as $row){
                                $feed = $row["feed"];
                                $title = $row["title"];
                                if( $i == 0 ){
                                    $active_class = 'active';
                                }else{
                                    $active_class = '';
                                }
                                ?>
                                <div class="item <?php echo $active_class; ?> ">
                                    <p>
                                        <b> <?php echo $title; ?> </b>
                                    </p>

                                    <p> <?php echo $feed; ?> </p>
                                </div>
                            <?php $i++; }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="callaction">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="aligncenter"><h1 class="aligncenter">Welcome To SLII</h1><span class="clear spacer_responsive_hide_mobile " style="height:13px;display:block;"></span>The Sri Lanka Insurance Institute is the only educational body for those in the local
                        insurance field. It was established in 1981 to enhance professionalism in the practice of
                        insurance in Sri Lanka and develop the necessary human resources in insurance and other
                        related financial services. Over the years the institute has been governed by various
                        councils and now comprises of 11 dynamic individuals from the field of insurance. The
                        institute was established to fulfill the need to create and autonomous educational body
                        devoted to enhancing the standards of the insurance field in Sri Lanka.</div>
                </div>
            </div>
        </div>
    </section>
    <section id="content">
        <div class="container">
            <div class="row">
                <div class="skill-home"> <div class="skill-home-solid clearfix">
                        <div class="col-md-3 text-center">
                            <span class="icons c1"><i class="fa fa-trophy"></i></span> <div class="box-area">
                                <h3>Development</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icons c2"><i class="fa fa-picture-o"></i></span> <div class="box-area">
                                <h3>Professional Training</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icons c3"><i class="fa fa-desktop"></i></span> <div class="box-area">
                                <h3>COURSES</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p></div>
                        </div>
                        <div class="col-md-3 text-center">
                            <span class="icons c4"><i class="fa fa-globe"></i></span> <div class="box-area">
                                <h3>Exsams</h3> <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolores quae porro consequatur aliquam, incidunt eius magni provident</p>
                            </div></div>
                    </div></div>
            </div>
        </div>
        <!-- Portfolio Projects -->
        <section class="lates-work-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="heading">Recent Works</h4>
                        <div class="row">
                            <section id="projects">
                                <ul id="thumbs" class="portfolio">
                                    <li class="col-lg-3 design" data-id="id-0" data-type="web">
                                        <div class="item-thumbs">
                                            <img src="<?php echo base_url('assets/'.THEME);?>img/front/works/1.jpg" alt=""><br>
                                            <p>Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et.</p>
                                        </div>
                                    </li>
                                    <!-- Item Project and Filter Name -->
                                    <li class="item-thumbs col-lg-3 design" data-id="id-1" data-type="icon">
                                        <img src="<?php echo base_url('assets/'.THEME);?>img/front/works/2.jpg" alt=""><br>
                                        <p>Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et.</p>
                                    </li>
                                    <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                        <img src="<?php echo base_url('assets/'.THEME);?>img/front/works/3.jpg" alt=""><br>
                                        <p>Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et.</p>
                                    </li>

                                    <li class="item-thumbs col-lg-3 photography" data-id="id-2" data-type="illustrator">
                                        <img src="<?php echo base_url('assets/'.THEME);?>img/front/works/4.jpg" alt=""><br>
                                        <p>Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et.</p>
                                    </li>
                                    <!-- End Item Project -->
                                </ul>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <section class="se-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <h2>Modern Business Features</h2>
                    <p>The Modern Business template by Start Bootstrap includes:</p>
                    <ul>
                        <li>
                            <strong>Bootstrap v4</strong>
                        </li>
                        <li>jQuery</li>
                        <li>Font Awesome</li>
                        <li>Working contact form with validation</li>
                        <li>Unstyled page elements for easy customization</li>
                    </ul>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis, omnis doloremque non cum id reprehenderit, quisquam totam aspernatur tempora minima unde aliquid ea culpa sunt. Reiciendis quia dolorum ducimus unde.</p>
                </div>
                <div class="col-lg-6">
                    <img class="img-fluid rounded" src="http://placehold.it/700x450" alt="">
                </div>
            </div>
        </div>
    </section>
</div>
<a href="#" class="scrollup"><i class="fa fa-angle-up active"></i></a>