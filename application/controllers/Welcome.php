<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends FrontendController{


    public function __construct() {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->model('Ins_Comp_model');
        $this->load->library('AuthLib');
        $this->load->library('excel');
        $this->load->library('Datatables');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->session->set_userdata('active_menu_front', '1');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        $activities = $this->Admin_model->get_activities();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $videos = $this->Admin_model->get_all_videos($where);
        $this->render_layout('front/index',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds , 'videos' => $videos , 'recent_works' => $recent_works , 'activities' => $activities ),'front');
    }

    public function about_us(){
        $this->session->set_userdata('active_menu_front', '2');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $this->render_layout('front/about_us',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds , 'recent_works' => $recent_works),'front');
    }

    public function services(){
        $this->session->set_userdata('active_menu_front', '3');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        //$services = $this->Admin_model->get_all_services();
        $activities = $this->Admin_model->get_activities();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $this->render_layout('front/services',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds, 'recent_works' => $recent_works , 'activities' => $activities),'front');
    }

    public function portfolio(){
        add_asset(
            array('footer_js' => array('app/js/portfolio.js'),
            )
        );
        $this->session->set_userdata('active_menu_front', '4');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        $portfolio = $this->Admin_model->change_portfolio();
        $albums = $this->Admin_model->get_albums();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $this->render_layout('front/portfolio',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds, 'portfolio' => $portfolio , 'albums' => $albums , 'recent_works' => $recent_works  ),'front');
    }

    public function courses(){
        $this->session->set_userdata('active_menu_front', '5');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $this->render_layout('front/courses',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds , 'recent_works' => $recent_works),'front');
    }

    public function contact(){
        $this->session->set_userdata('active_menu_front', '6');
        $slider_images = $this->Admin_model->get_sliders();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        add_asset(
            array('footer_js' => array('app/js/contact.js')
            )
        );
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $this->render_layout('front/contact',array('slider_images' => $slider_images , 'news_feeds' => $news_feeds , 'recent_works' => $recent_works),'front');
    }

    public function member_register(){
        $this->session->set_userdata('active_menu_front', '8');
        $member_user_id = $this->input->get('id');
        $member_data = array();
        $member_data = $this->Ins_Comp_model->get_insurance_member_details($member_user_id);
        $member_exp_data = $this->Ins_Comp_model->get_insurance_member_exp_details($member_user_id);
        $insurance_cmp_data = $this->Ins_Comp_model->get_insurance_company_all();
        $news_feeds = $this->Admin_model->get_all_news_feeds();
        $where = array(
            'status' => 1
        );
        $recent_works = $this->Admin_model->recent_works($where);
        $member_data['surname'] =   isset($member_data['surname']) ? explode(',' , $member_data['surname']) : array();
        $member_data['current_industry_nature'] = isset($member_data['current_industry_nature']) ? explode(',' , $member_data['current_industry_nature']) : array();
        $member_data['insurance_type'] = isset($member_data['insurance_type']) ? explode(',' , $member_data['insurance_type']) : array();
        $member_data['ins_areas'] = isset($member_data['ins_areas']) ? explode(',' , $member_data['ins_areas']) : array();
        $member_data['edu_qualifications'] = isset($member_data['edu_qualifications']) ? explode(',' , $member_data['edu_qualifications']) : array();
        $member_data['slii_member_edit_id'] = isset($member_data['slii_member_edit_id']) ? $member_data['slii_member_edit_id'] : '';
        $member_data['pref_mail_adr'] = isset($member_data['pref_mail_adr']) ? explode(',' , $member_data['pref_mail_adr']) : array();
        add_asset(
            array('footer_js' => array('app/js/register_user_front.js')
            )
        );
        $this->render_layout('front/member_register',array('member_data'=> $member_data , 'member_exp_data' => $member_exp_data , 'ins_cmp_data' => $insurance_cmp_data , 'news_feeds' => $news_feeds , 'recent_works' => $recent_works), 'front_register' );
    }
}
