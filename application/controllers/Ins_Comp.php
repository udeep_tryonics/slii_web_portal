<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ins_Comp extends FrontendController
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Ins_Comp_model');
        $this->load->library('AuthLib');
        $this->load->library('excel');
        $this->load->library('Datatables');
        $this->load->library('form_validation');
    }

    public function profile(){
        $this->authlib->has_rights('INSURANCE_COMPANY');
        $this->render_layout('ins_comp/profile',array());
    }

    public function add_members(){

        $member_user_id = $this->input->get('id');
        $member_data = array();
        $member_data = $this->Ins_Comp_model->get_insurance_member_details($member_user_id);
        $member_exp_data = $this->Ins_Comp_model->get_insurance_member_exp_details($member_user_id);
        $member_data['surname'] =   isset($member_data['surname']) ? explode(',' , $member_data['surname']) : array();
        $member_data['current_industry_nature'] = isset($member_data['current_industry_nature']) ? explode(',' , $member_data['current_industry_nature']) : array();
        $member_data['insurance_type'] = isset($member_data['insurance_type']) ? explode(',' , $member_data['insurance_type']) : array();
        $member_data['ins_areas'] = isset($member_data['ins_areas']) ? explode(',' , $member_data['ins_areas']) : array();
        $member_data['edu_qualifications'] = isset($member_data['edu_qualifications']) ? explode(',' , $member_data['edu_qualifications']) : array();
        $member_data['slii_member_edit_id'] = isset($member_data['slii_member_edit_id']) ? $member_data['slii_member_edit_id'] : '';
        $member_data['pref_mail_adr'] = isset($member_data['pref_mail_adr']) ? explode(',' , $member_data['pref_mail_adr']) : array();

        $this->authlib->has_rights('INSURANCE_COMPANY');
        add_asset(
            array('footer_js' => array('app/js/add_members.js')
            )
        );

        $this->render_layout('ins_comp/add_members',array('member_data'=> $member_data , 'member_exp_data' => $member_exp_data ) );
    }

    public function members(){
        $this->authlib->has_rights('INSURANCE_COMPANY');
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/ins_members.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );

        $this->render_layout('ins_comp/members',array());
    }

    public function get_ins_users_list(){
        $login_data = $this->session->userdata('login_data');
        $this->datatables->select('su.id,su.username,sm.id as slii_member_id,su.is_active');
        $this->datatables->from('slii_users su');
        $this->datatables->join('slii_user_group_pivot sug', 'su.id = sug.fk_user_id' , 'left');
        $this->datatables->join('slii_member sm', 'sm.fk_slii_user_id = su.id' , 'left');
        $this->datatables->where('su.company' , $login_data['company']);
        $this->datatables->where('sug.fk_group_id' , 5);
        echo $this->datatables->generate();
    }


    public function member_registration_submit(){

        $this->form_validation->set_rules('fullname','Full Name','required');
        $this->form_validation->set_rules('surname[]','Surname','required');
        $this->form_validation->set_rules('edu_qualifications[]','Educational Qualifications','required');
        $this->form_validation->set_rules('current_industry_nature[]','Current Industry Nature','required');
        $this->form_validation->set_rules('insurance_type[]','Insurance Type','required');
        $this->form_validation->set_rules('cmp_email','Company Email','required|valid_email');
        $this->form_validation->set_rules('home_email','Home Email','required|valid_email');

        $response = array('type' => 'error', 'message' => '');
        if($this->form_validation->run() !== false) {

            $login_data = $this->session->userdata('login_data');
            $slii_member_edit_id = $this->input->post('slii_member_edit_id');
            $fullname = $this->input->post('fullname');
            $surname = $this->input->post('surname');
            $dob = $this->input->post('dob');
            $nic = $this->input->post('nic');
            $cmp_name_adr = $this->input->post('cmp_name_adr');
            $cmp_phone = $this->input->post('cmp_phone');
            $cmp_email = $this->input->post('cmp_email');
            $home_adr = $this->input->post('home_adr');
            $home_phone = $this->input->post('home_phone');
            $home_email = $this->input->post('home_email');
            $home_mobile = $this->input->post('home_mobile');
            $present_occupation = $this->input->post('present_occupation');
            $edu_qualifications = implode(',', $this->input->post('edu_qualifications'));
            $ins_areas = implode(',', $this->input->post('ins_areas'));
            $current_industry_nature = implode(',', $this->input->post('current_industry_nature'));
            $insurance_type = implode(',', $this->input->post('insurance_type'));
            $pref_mail_adr = implode(',', $this->input->post('pref_mail_adr'));
            $surname = implode(',', $this->input->post('surname'));
            $experience_list = $this->input->post('experience_list');
            $payment_type = $this->input->post('payment_type');
            $company = $this->input->post('company');

            if(!$company){
                $company = $login_data['company'];
            }

            $member_data = array(
                'fullname' => $fullname,
                'surname' => $surname,
                'dob' => $dob,
                'nic' => $nic,
                'cmp_name_adr' => $cmp_name_adr,
                'cmp_phone' => $cmp_phone,
                'cmp_email' => $cmp_email,
                'home_adr' => $home_adr,
                'home_phone' => $home_phone,
                'home_email' => $home_email,
                'present_occupation' => $present_occupation,
                'edu_qualifications' => $edu_qualifications,
                'ins_areas' => $ins_areas,
                'current_industry_nature' => $current_industry_nature,
                'insurance_type' => $insurance_type,
                'pref_mail_adr' => $pref_mail_adr,
                'home_mobile' => $home_mobile,
            );
            $register_data = array(
                'username' => $fullname,
                'password' => '202cb962ac59075b964b07152d234b70',
                'company' => $company,
                'is_active' => 0
            );
            $response = $this->Ins_Comp_model->member_registration_submit($member_data,$register_data,$slii_member_edit_id,$experience_list);

        }
        else{
            $response['type'] = 'error';
            $response['message'] = validation_errors();
        }

        echo json_encode($response);
    }

}
