<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends FrontendController
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Admin_model');
        $this->load->library('AuthLib');
        $this->load->library('excel');
        $this->load->library('Datatables');
        $this->load->library('form_validation');
    }

    public function profile(){
        $this->session->set_userdata('active_menu', '8');
        $this->authlib->has_rights('SLII_ADMIN');
        $this->render_layout('admin/profile',array());
    }

    public function user_mgt(){
        $this->authlib->has_rights('SLII_ADMIN');
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/user_mgt.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/user_mgt',array());
    }

    public function activities(){
        $this->session->set_userdata('active_menu', '6');
        $this->authlib->has_rights('SLII_ADMIN');
        $activities = $this->Admin_model->recent_works(array());
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/activities.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/activities', array('activities' => $activities) );
    }

    public function get_users_list(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select("su.id,
            su.username,
            sg.description,
            su.is_active,
            IF(sc.cmp_name IS NULL,'Individual',sc.cmp_name) 
            AS company
            ");
            $this->datatables->from('slii_users su');
            $this->datatables->join('slii_user_group_pivot sug', 'su.id = sug.fk_user_id' , 'left');
            $this->datatables->join('slii_groups sg', 'sg.id = sug.fk_group_id' , 'left');
            $this->datatables->join('slii_company sc', 'sc.cmp_id = su.company' , 'left');
            echo $this->datatables->generate();
        }
    }

    function change_sliders(){
        $this->session->set_userdata('active_menu', '1');
        $data = array();
        if($this->input->post('upload') && !empty($_FILES['userfiles']['name'])){
            $filesCount = count($_FILES['userfiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userfiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userfiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userfiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userfiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userfiles']['size'][$i];

                $uploadPath = 'assets/default/img/front/slides/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite']     = true;
                $config['min_width'] = 1440;
                $config['min_height'] = 520;
                $config['max_width'] = 1440;
                $config['max_height'] = 520;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                    $array_images = array(
                        'file_name' => $fileData['file_name']
                    );
                    $response = $this->Admin_model->add_slider_image($array_images);
                    $this->session->set_flashdata('sp_message', $response);
                }else{
                    $sp_message = array('type' => 'error', 'message' => $this->upload->display_errors());
                    $this->session->set_flashdata('sp_message', $sp_message);
                }
            }
            redirect('Admin/change_sliders');
        }
        else{
            $this->authlib->has_rights('SLII_ADMIN');
            $slider_images = $this->Admin_model->change_sliders();
            add_asset(
                array('footer_js' => array('app/js/change_sliders.js'),
                )
            );
            $this->render_layout('admin/change_sliders', array('slider_images'=> $slider_images));
        }
    }

    public function remove_slider_img(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $img_id = $this->input->post('image_id');
            $response = $this->Admin_model->remove_slider_img($img_id);
            echo json_encode($response);
        }
    }

    public function remove_gallery_img(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $img_id = $this->input->post('image_id');
            $response = $this->Admin_model->remove_gallery_img($img_id);
            echo json_encode($response);
        }
    }

    public function remove_recent_works_img(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $img_id = $this->input->post('image_id');
            $response = $this->Admin_model->remove_recent_works_img($img_id);
            echo json_encode($response);
        }
    }

    public function add_news_feeds(){
        $this->session->set_userdata('active_menu', '3');
        $this->authlib->has_rights('SLII_ADMIN');
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/add_news_feeds.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/add_news_feeds', array() );
    }

    public function get_slii_albums(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('id,album_name,album_image,status');
            $this->datatables->from('slii_albums');
            echo $this->datatables->generate();
        }
    }

    public function get_activities(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('id,title,feed,icon,title_short');
            $this->datatables->from('slii_activities');
            echo $this->datatables->generate();
        }
    }

    function add_gallery_images(){
        $this->session->set_userdata('active_menu', '2');
        $data = array();
        $album_id = $this->input->post('album');
        $image_title = $this->input->post('image_title');
        if($this->input->post('upload') && !empty($_FILES['userfiles']['name'])){
            $this->form_validation->set_rules('album','Album','required');
            $this->form_validation->set_rules('image_title','Image Title','required');
            if($this->form_validation->run() !== false) {
                $filesCount = count($_FILES['userfiles']['name']);
                for($i = 0; $i < $filesCount; $i++){
                    $_FILES['userFile']['name'] = $_FILES['userfiles']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['userfiles']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['userfiles']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['userfiles']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['userfiles']['size'][$i];

                    $uploadPath = 'assets/default/img/front/gallery/';
                    $config['upload_path'] = $uploadPath;
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['overwrite']     = true;

//                    $config['min_width'] = 820;
//                    $config['min_height'] = 520;
//                    $config['max_width'] = 830;
//                    $config['max_height'] = 550;

                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if($this->upload->do_upload('userFile')){
                        $fileData = $this->upload->data();
                        $uploadData[$i]['file_name'] = $fileData['file_name'];
                        $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                        $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                        $array_images = array(
                            'file_name' => $fileData['file_name'],
                            'album_id' => $album_id,
                            'title' => $image_title
                        );
                        $response = $this->Admin_model->add_gallery_image($array_images);
                        $this->session->set_flashdata('sp_message', $response);
                    }else{
                        $sp_message = array('type' => 'error', 'message' => $this->upload->display_errors());
                        $this->session->set_flashdata('sp_message', $sp_message);
                    }
                }
                redirect('Admin/add_gallery_images');
            }else{
                $response['type'] = 'error';
                $response['message'] = validation_errors();
                $this->session->set_flashdata('sp_message', $response);
                redirect('Admin/add_gallery_images');
            }
        }
        else{
            $this->authlib->has_rights('SLII_ADMIN');
            $gallery_images = $this->Admin_model->photo_gallery();
            $albums = $this->Admin_model->get_albums();

            add_asset(
                array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/change_gallery_images.js'),
                    'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
                )
            );

            $this->render_layout('admin/photo_gallery', array('gallery_images'=> $gallery_images , 'albums' => $albums ));
        }
    }

    public function get_news_feeds(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('snf.id, snf.feed , snf.title');
            $this->datatables->from('slii_news_feeds snf');
            echo $this->datatables->generate();
        }
    }

    public function get_all_services(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('id, title , description , icon');
            $this->datatables->from('slii_services');
            echo $this->datatables->generate();
        }
    }

    public function add_feed(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->form_validation->set_rules('feed','News Feed','required');
            $this->form_validation->set_rules('title','News Title','required');
            if($this->form_validation->run() !== false) {
                $news_feed = $this->input->post('feed');
                $news_title = $this->input->post('title');
                $news_id = $this->input->post('feed_id');
                $news_data = array(
                    'feed' => $news_feed,
                    'title' => $news_title,
                    'id' => $news_id
                );
                $response = $this->Admin_model->add_feed($news_data);
            }else{
                $response['type'] = 'error';
                $response['message'] = validation_errors();
            }
            echo json_encode($response);
        }
    }

    public function add_activities(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->form_validation->set_rules('feed','News Feed','required');
            $this->form_validation->set_rules('title','News Title','required');
            $this->form_validation->set_rules('title_short','News Title Short','required');
            $this->form_validation->set_rules('icon','Icon','required');
            if($this->form_validation->run() !== false) {
                $news_feed = $this->input->post('feed');
                $news_title = $this->input->post('title');
                $news_title_short = $this->input->post('title_short');
                $news_id = $this->input->post('feed_id');
                $icon = $this->input->post('icon');
                $news_data = array(
                    'feed' => $news_feed,
                    'title' => $news_title,
                    'title_short' => $news_title_short,
                    'id' => $news_id,
                    'icon' => $icon
                );
                $response = $this->Admin_model->add_activities($news_data);
            }else{
                $response['type'] = 'error';
                $response['message'] = validation_errors();
            }
            echo json_encode($response);
        }
    }

    public function delete_news_feed(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $feed_id = $this->input->post('feed_id');
            $where_array = array(
                'id' => $feed_id
            );
            $response = $this->Admin_model->delete_news_feed($where_array);
            echo json_encode($response);
        }
    }

    public function delete_service(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $id = $this->input->post('id');
            $where_array = array(
                'id' => $id
            );
            $response = $this->Admin_model->delete_service($where_array);
            echo json_encode($response);
        }
    }

    public function set_active_album(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $id = $this->input->post('id');
            $type = $this->input->post('type');
            $where_array = array(
                'id' => $id,
            );
            $data_array = array(
                'status' => $type
            );
            $response = $this->Admin_model->set_active_album($where_array , $data_array);
            echo json_encode($response);
        }
    }

    public function delete_album(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $album_id = $this->input->post('album_id');
            $where_array = array(
                'id' => $album_id
            );
            $response = $this->Admin_model->delete_album($where_array);
            echo json_encode($response);
        }
    }

    public function delete_activities(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $feed_id = $this->input->post('feed_id');
            $where_array = array(
                'id' => $feed_id
            );
            $response = $this->Admin_model->delete_activities($where_array);
            echo json_encode($response);
        }
    }

    public function courses(){
        $this->authlib->has_rights('SLII_ADMIN');
        $subjects = $this->Admin_model->get_all_subjects();
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/courses.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/courses', array('subjects' => $subjects));
    }

    public function subjects(){
        $this->authlib->has_rights('SLII_ADMIN');
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/subjects.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/subjects', array());
    }

    public function get_slii_courses(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('*');
            $this->datatables->from('slii_courses sc');
            echo $this->datatables->generate();
        }
    }

    public function get_slii_subjects(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('*');
            $this->datatables->from('slii_subjects sc');
            echo $this->datatables->generate();
        }
    }

    public function activate_user(){
        if(true){
            $member_id = $this->input->post('id');
            $where_array = array(
                'id' => $member_id
            );
            $update_array = array(
                'is_active' => 1
            );
            $response = $this->Admin_model->activate_user($where_array, $update_array);
            echo json_encode($response);
        }
    }

    public function add_subject(){
        $subject_code = $this->input->post('subject_code');
        $subject_name = $this->input->post('subject_name');
        $subject_data = array(
            'subject_code' => $subject_code,
            'subject_name' => $subject_name
        );
        $response = $this->Admin_model->add_subject($subject_data);
        echo json_encode($response);
    }

    public function add_course(){
        $course_subjects = $this->input->post('course_subjects');
        $course_name = $this->input->post('course_name');
        $course_code = $this->input->post('course_code');
        $course_id = $this->input->post('course_id');
        $course_data = array(
            'subjects' => implode(',', $course_subjects),
            'course_description' => $course_code,
            'course_name' => $course_name
        );
        $response = $this->Admin_model->add_course($course_data,$course_id);
        echo json_encode($response);
    }

    public function course_payment(){
        $this->authlib->has_rights('SLII_ADMIN');
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/course_payment.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/course_payment',array());
    }

    public function get_course_payment(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('smcp.id,su.username,sc.course_name,sc.course_fee, (sc.course_fee - COALESCE(SUM(scp.amount),0)) as outstanding');
            $this->datatables->from('slii_member_course_pivot smcp');
            $this->datatables->join('slii_courses sc', 'sc.id = smcp.fk_course_id' , 'left');
            $this->datatables->join('slii_users su', 'su.id = smcp.fk_member_id' , 'left');
            $this->datatables->join('slii_course_payment scp', 'scp.fk_mcp_id = smcp.id' , 'left');
            $this->datatables->group_by('smcp.id');
            echo $this->datatables->generate();
        }
    }

    public function get_messages(){
        if($this->authlib->check_has_rights('SLII_ADMIN')){
            $this->datatables->select('id,name,email,subject,message');
            $this->datatables->from('slii_contact_informations');
            echo $this->datatables->generate();
        }
    }

    public function get_payments_for_course(){
        $course_payment_id = $this->input->post('id');
        $where_array = array(
            'fk_mcp_id' => $course_payment_id
        );
        $response = $this->Admin_model->get_payments_for_course($where_array);
        echo json_encode($response);
    }

    public function add_contact_details(){
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $subject = $this->input->post('subject');
        $message = $this->input->post('message');

        $this->form_validation->set_rules('name','Name','required');
        $this->form_validation->set_rules('email','Email','required|valid_email');
        $this->form_validation->set_rules('subject','Subject','required');
        $this->form_validation->set_rules('message','Message','required');

        if($this->form_validation->run() !== false) {
            $contact_details = array(
                'name' => $name,
                'email' => $email,
                'subject' => $subject,
                'message' => $message
            );
            $response = $this->Admin_model->add_contact_details($contact_details);
        }else{
            $response['type'] = 'error';
            $response['message'] = validation_errors();
        }

        echo json_encode($response);
    }

    public function update_gallery_img(){
        $id = $this->input->post('image_id');
        $title = $this->input->post('title');
        $update_data = array(
            'title' => $title
        );
        $where = array(
            'id' => $id
        );
        $response = $this->Admin_model->update_gallery_img($update_data , $where);
        echo json_encode($response);
    }

    public function update_recent_works_img(){
        $id = $this->input->post('image_id');
        $title = $this->input->post('title');
        $update_data = array(
            'title' => $title
        );
        $where = array(
            'id' => $id
        );
        $response = $this->Admin_model->update_recent_works_img($update_data , $where);
        echo json_encode($response);
    }

    public function set_active_image(){
        $id = $this->input->post('image_id');
        $status = $this->input->post('status');
        $update_data = array(
            'status' => $status
        );
        $where = array(
            'id' => $id
        );
        $response = $this->Admin_model->set_active_image($update_data , $where);
        echo json_encode($response);
    }

    public function set_active_recent_works(){
        $id = $this->input->post('image_id');
        $status = $this->input->post('status');
        $update_data = array(
            'status' => $status
        );
        $where = array(
            'id' => $id
        );
        $response = $this->Admin_model->set_active_recent_works($update_data , $where);
        echo json_encode($response);
    }

    public function set_active_slider(){
        $id = $this->input->post('image_id');
        $status = $this->input->post('status');
        $update_data = array(
            'status' => $status
        );
        $where = array(
            'id' => $id
        );
        $response = $this->Admin_model->set_active_slider($update_data , $where);
        echo json_encode($response);
    }

    public function videos(){
        $this->session->set_userdata('active_menu', '4');
        $this->authlib->has_rights('SLII_ADMIN');
        $videos = $this->Admin_model->get_all_videos(array());
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/videos.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/videos', array('videos' => $videos) );
    }

    public function add_video(){

        $this->form_validation->set_rules('video_url','Video Url','required');

        if($this->form_validation->run() !== false) {
            $video_url = $this->input->post('video_url');
            $video_details = array(
                'video_url' =>  $video_url
            );
            $response = $this->Admin_model->add_video($video_details);
        }else{
            $response['type'] = 'error';
            $response['message'] = validation_errors();
        }
        echo json_encode($response);
    }

    public function set_active_video(){
        $id = $this->input->post('video_id');
        $status = $this->input->post('status');
        $update_data = array(
            'status' => $status
        );
        $update_data_inactive = array(
            'status' => 0
        );
        $where = array(
            'id' => $id
        );
        $where_inactive = array(
            'id !=' => $id
        );
        $response = $this->Admin_model->set_active_video($update_data , $where , $where_inactive , $update_data_inactive);
        echo json_encode($response);
    }

    public function remove_video(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $video_id = $this->input->post('video_id');
            $where_array = array(
                'id' => $video_id
            );
            $response = $this->Admin_model->remove_video($where_array);
            echo json_encode($response);
        }
    }

    public function recent_works(){
        $this->session->set_userdata('active_menu', '5');
        $data = array();
        if($this->input->post('upload') && !empty($_FILES['userfiles']['name'])){
            $filesCount = count($_FILES['userfiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userfiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userfiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userfiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userfiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userfiles']['size'][$i];

                $uploadPath = 'assets/default/img/front/works/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite']     = true;
                $config['min_width'] = 820;
                $config['min_height'] = 520;
                $config['max_width'] = 830;
                $config['max_height'] = 550;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                    $array_images = array(
                        'file_name' => $fileData['file_name']
                    );
                    $response = $this->Admin_model->add_recent_works_image($array_images);
                    $this->session->set_flashdata('sp_message', $response);
                }else{
                    $sp_message = array('type' => 'error', 'message' => $this->upload->display_errors());
                    $this->session->set_flashdata('sp_message', $sp_message);
                }
            }
            redirect('Admin/recent_works');
        }
        else{
            $this->authlib->has_rights('SLII_ADMIN');
            $recent_works_images = $this->Admin_model->recent_works(array());
            add_asset(
                array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/recent_works.js'),
                    'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
                )
            );
            $this->render_layout('admin/recent_works', array('recent_works_images' => $recent_works_images) );
        }

    }

    public function messages(){
        $this->session->set_userdata('active_menu', '7');
        $this->authlib->has_rights('SLII_ADMIN');
        $videos = $this->Admin_model->get_all_videos(array());
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/messages.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/messages' , array());
    }

    public function services(){
        $this->session->set_userdata('active_menu', '10');
        $this->authlib->has_rights('SLII_ADMIN');
        $videos = $this->Admin_model->get_all_videos(array());
        add_asset(
            array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/services.js'),
                'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
            )
        );
        $this->render_layout('admin/services' , array());
    }

    public function albums(){

        $this->session->set_userdata('active_menu', '9');
        $data = array();
        $album_name = $this->input->post('album_name');
        if($this->input->post('upload') && !empty($_FILES['userfiles']['name'])){
            $filesCount = count($_FILES['userfiles']['name']);
            for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userfiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userfiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userfiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userfiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userfiles']['size'][$i];

                $uploadPath = 'assets/default/img/front/albums/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|png';
                $config['overwrite']     = true;
//                $config['min_width'] = 820;
//                $config['min_height'] = 520;
//                $config['max_width'] = 830;
//                $config['max_height'] = 550;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['created'] = date("Y-m-d H:i:s");
                    $uploadData[$i]['modified'] = date("Y-m-d H:i:s");
                    $array_images = array(
                        'album_image' => $fileData['file_name'],
                        'album_name' => $album_name
                    );
                    $response = $this->Admin_model->add_albums($array_images);
                    $this->session->set_flashdata('sp_message', $response);
                }else{
                    $sp_message = array('type' => 'error', 'message' => $this->upload->display_errors());
                    $this->session->set_flashdata('sp_message', $sp_message);
                }
            }
            redirect('Admin/albums');
        }
        else{
            $this->authlib->has_rights('SLII_ADMIN');
            add_asset(
                array('footer_js' => array('bower_components/datatables.net/js/jquery.dataTables.min.js','bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js','app/js/albums.js'),
                    'header_css'=>array('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')
                )
            );
            $this->render_layout('admin/albums' , array());
        }

    }

    public function get_album_images(){
        $id = $this->input->post('id');
        $where_array = array(
            'sa.id' => $id,
            'sgi.status' => 1
        );
        $response = $this->Admin_model->get_album_images($where_array);
        echo json_encode($response);
    }

    public function add_services(){
        if($this->authlib->check_has_rights('SLII_ADMIN')) {
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $icon = $this->input->post('icon');
            $data_array = array(
                'title' => $title,
                'description' => $description,
                'icon' => $icon
            );
            $response = $this->Admin_model->add_services($data_array);
            echo json_encode($response);
        }
    }

}
