<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends FrontendController
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Member_model');
        $this->load->library('AuthLib');
        $this->load->library('excel');
        $this->load->library('Datatables');
        $this->load->library('form_validation');
    }

    public function profile(){
        $this->authlib->has_rights('SLII_MEMBER');
        $courses = $this->Member_model->get_my_courses();
        if(!$courses){
            $courses = array();
        }
        $this->render_layout('member/profile',array('courses' => $courses));
    }

    public function apply_for_courses(){
        $this->authlib->has_rights('SLII_MEMBER');
        add_asset(
            array('footer_js' => array('app/js/apply_for_courses.js')
            )
        );
        $courses = $this->Member_model->get_all_courses();
        $this->render_layout('member/apply_for_courses',array('courses' => $courses));
    }

    public function exams(){
        $this->authlib->has_rights('SLII_MEMBER');
        $this->render_layout('member/exams',array());
    }

    public function assign_for_courses(){
        $login_data = $this->session->userdata('login_data');
        $course_id = $this->input->post('course_id');
        $assign_subjects = $this->input->post('assign_subjects');
        $course_data = array(
            'fk_course_id' => $course_id,
            'fk_member_id' => $login_data['id'],
            'assign_subjects' => implode(',',$assign_subjects)
        );
        $response = $this->Member_model->assign_for_courses($course_data);
        echo json_encode($response);
    }

    public function get_subjects_for_course(){
        $course_id = $this->input->post('course_id');
        $where_data = array(
            'id' => $course_id
        );
        $response = $this->Member_model->get_subjects_for_course($where_data);
        echo json_encode($response);
    }

    public function test(){
        $courses = $this->Member_model->get_my_courses();
        print_r($courses);
    }

}
