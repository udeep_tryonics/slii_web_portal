<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Title           Auth Controller
 *
 * @package        Spark
 * Location        application/controllers/user/Auth.php
 *
 * @author         ruwan - <ruwanpathmalal@gmail.com>
 * @copyright      Spark
 *
 * created on      27/09/2017, 12:21 PM by ruwan
 *
 * Description     Consist of authentication request handling methods
 *
 * */
class Auth extends FrontendController {

    /**
     * Auth controller.
     *
     */

    public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('AuthLib');
    }

    public function index(){
        $this->session->set_userdata('active_menu_front', '7');
        if(!$this->authlib->is_user_logged()){
            $view_data = array();
            if($this->input->post()){
                $this->form_validation->set_rules('username','Username','required');
                $this->form_validation->set_rules('password','Password','required');
                if($this->form_validation->run() !== false){
                    $username = $this->input->post('username');
                    $password = $this->input->post('password');
                    $data = $this->authlib->signin($username,$password);
                    if(!empty($data)){
                        $this->authlib->init_user_session($data);
                        $this->authlib->user_landing();
                    }else {
                        // Invalid user name or password
                        $sp_message = array('type'=>'error','message'=>'Invalid user name or password.');
                        $view_data['sp_message'] = $sp_message;
                    }
                }else{
                    //Please enter valid user name and password.
                    $sp_message = array('type'=>'error','message'=>'Please enter valid user name and password.');
                    $view_data['sp_message'] = $sp_message;
                }
            }
            $this->render_layout('user/login',$view_data,'login');
            //print_r($this->session->userdata('login_data'));
        }else{
            $this->authlib->user_landing();
            //print_r($this->session->userdata('login_data'));
        }

    }

    public function logout(){
        $this->authlib->logout_user();
        redirect('user/auth');
    }

    public function password_reset(){
        if($this->authlib->is_user_logged()) {
            $view_data = array();
            if ($this->input->post()) {
                $this->form_validation->set_rules('password','Password','required');
                $this->form_validation->set_rules('retype_password','Retype Password','required');
                if($this->form_validation->run() !== false) {
                    $password = $this->input->post('password');
                    $retype_password = $this->input->post('retype_password');
                    if ($password == $retype_password) {
			            $userdata = $this->session->userdata('login_data');
                        $this->authlib->reset_password_user($password,$userdata['username']);
                        $sp_message = array('type' => 'success', 'message' => 'Successfully Updated.');
                        $view_data['sp_message'] = $sp_message;
                        $this->render_layout('user/password_reset', $view_data);
                    } else {
                        $sp_message = array('type' => 'error', 'message' => 'Mismatched Passwords.');
                        $view_data['sp_message'] = $sp_message;
                        $this->render_layout('user/password_reset', $view_data);
                    }
                }
                else {
                    $sp_message = array('type' => 'error', 'message' => 'Please Enter Your Passwords.');
                    $view_data['sp_message'] = $sp_message;
                    $this->render_layout('user/password_reset', $view_data);
                }
            } else {
                $this->render_layout('user/password_reset', $view_data);
            }
        }else{
            $this->authlib->user_landing();
        }
    }

    public function get_user_assigned_group(){
        $auth_set = $this->authlib->get_user_assigned_group();
        print_r($auth_set);
    }

}
