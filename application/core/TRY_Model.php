<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * SparkBase Model.
 *
 * Base model of the application.
 *
 */
class TRY_Model extends CI_Model {
    function __construct(){
        parent::__construct();
        $this->core_write = $this->load->database('default', TRUE);
        $this->core_read = $this->load->database('default', TRUE);
    }
}