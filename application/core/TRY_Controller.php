<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/**
 * SparkBase controller.
 *
 * Base controller of the application.
 *
 */
class SparkBaseController extends CI_Controller
{
    protected $_lang;
    protected $_user_time_zone;

    public function __construct()
    {
        parent::__construct();
        $this->setLocale();
        $this->setAppTimeZone();
        $this->setDateTimeLib();
    }

    private function setLocale(){
    }

    public function setAppTimeZone(){
        date_default_timezone_set("Asia/Colombo");
    }

    public function getUserTimeZone(){
    }

    private function setDateTimeLib(){

    }

    public function getDateTimeLib(){
        return $this->datetimelib;
    }

    public function render_layout($view,$data,$layout='application'){
        $data['current_view'] = THEME.$view;
        $this->load->view(THEME.'layouts/'.$layout,$data);
    }
}

/**
 * Front controller.
 *
 * Front controller of the application.
 *
 */
class FrontendController extends SparkBaseController
{
    public function __construct()
    {
        parent::__construct();
        // $this->__set_notifications();
        // $this->__set_rns_rooms();
        // $this->_get_breaks();
    }

    private function __set_notifications(){
        $login_data = $this->session->userdata('login_data');
        $filters = array('tun.agent_id'=>$login_data['username']);
        $current_time = date("Y-m-d");
        $filters['DATE(tun.active_time_from) ='] = $current_time;
        $this->load->model('Agent_model');
        $notification_data = $this->Agent_model->get_notifications($filters);
        Registry::set('top_notification',$notification_data);
    }

    private function __set_rns_rooms(){
        $rooms =array();
        $this->load->library('AuthLib');
        $login_data = $this->session->userdata('login_data');
        if($this->authlib->check_has_rights('Admin')){
            $rooms[] = $login_data['id'];
            $rooms[] = 'admin';
        }elseif($this->authlib->check_has_rights('Agent')){
            $rooms[] = $login_data['id'];
        }
        Registry::set('rns_rooms',$rooms);
    }
    
    /*
     * @author gayani
     */
    private function _get_breaks()
    {
        $this->load->model('Agent_model');
        $login_data = $this->session->userdata('login_data');
        $user_id = $login_data['id'];

        if($this->authlib->check_has_rights('Agent')){

            //set session if not break is not finished
            $this->_set_session_for_not_finished($login_data);

            //get started break
            $started_break = $this->_get_started_break();


            if($user_id != ''){
                //get all breaks in system
                $date = Date('Y-m-d');
                $agent_id = $login_data['username'];
                $subquery = "(select max(created_at) from try_assign_breaks where break_id = a.break_id and Date(created_at) < '".$date."' and agent_id = '".$agent_id."')";
                $filters['subquery']['a.created_at'] = $subquery;
                $filters['a.agent_id'] = $agent_id;
                $breaks = $this->Agent_model->get_assign_breaks($filters);

                $unassign = $this->_get_unassign_breaks($breaks, $login_data);

                if(!empty($unassign['un_assign_breaks'])){
                    Registry::set('breaks',$unassign['un_assign_breaks']);
                }else if(empty($unassign['un_assign_keys'])){
                    Registry::set('breaks',$breaks);
                }else{
                    Registry::set('breaks',array());
                }

                Registry::set('started_break',$started_break);

            }
        }

    }


    private function  _set_session_for_not_finished($login_data)
    {
        $filters['t.agent_id'] = $login_data['username'];
        $filters['end_date_time'] = null;
        $tracks_not_finished = $this->Agent_model->get_schedule_tracker($filters);
        if($tracks_not_finished){
            $this->session->set_userdata(array('started_break' => $tracks_not_finished[0]['break_id']));
            $this->session->set_userdata(array('record_id' => $tracks_not_finished[0]['id']));
        }else{
            $this->session->unset_userdata('started_break');
            $this->session->unset_userdata('record_id');
        }
    }

    private function _get_started_break()
    {
        $started_break = '';
        $started_break_session = $this->session->userdata('started_break');
        if(!empty($started_break_session)){
            $filters['b.id'] = $this->session->userdata('started_break');
            $s_break = $this->Agent_model->get_assign_breaks($filters);
            if(!empty($s_break)){
                $started_break =$s_break[0]['break_name'];
            }
        }
        return $started_break;
    }

    private function _get_unassign_breaks($breaks, $login_data)
    {
        $data = array();
        $un_assign_keys = array();
        $un_assign_breaks = array();
        $today = Date("Y-m-d");
        $filters_2['t.agent_id'] = $login_data['username'];
        $filters_2['DATE_FORMAT(t.created_at,"%Y-%m-%d")'] = $today;
        $tracks = $this->Agent_model->get_schedule_tracker($filters_2);
        //get break id of unassign breaks
        if(!empty($breaks) && !empty($tracks)){
            for($i=0; $i < count($breaks); $i++){
                for($j=0; $j < count($tracks); $j++){
                    if($breaks[$i]['id'] == $tracks[$j]['break_id']){
                        array_push($un_assign_keys, $breaks[$i]['id']);
                    }
                }
            }
            if(!empty($un_assign_keys)){
                $filters['a.agent_id'] = $login_data['username'];
                $un_assign_breaks = $this->Agent_model->get_unassign_breaks($un_assign_keys,$filters);


            }
        }
        $data = array(
            'un_assign_breaks' => $un_assign_breaks,
            'un_assign_keys' => $un_assign_keys
        );
        return $data;
    }
}
