<?php
/**
 * Title           SP_Log
 *
 * @package        Spark-nearbypromo
 * Location        SP_Log.php
 *
 * @author         ruwan - <ruwanpathmalal@gmail.com>
 * @copyright      Spark (Pvt) Ltd
 *
 * created on     4/07/16, 4:55 PM by ruwan
 *
 * Description     Extending Codeigniter Log class
 *
 * */
class TRY_Log extends CI_Log {

    public function __construct()
    {
        parent::__construct();
        $this->_levels	= array('ERROR' => '1', 'DEBUG' => '2','INFO' => '3', 'ALL' => '4','APP_ERROR'=>'10','APP_SECURITY'=>'11','APP_INFO'=>'12','APP_DEBUG'=>'13',);
    }
}