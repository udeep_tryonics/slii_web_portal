<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_model extends TRY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_all_courses(){
        $this->core_read->select('sc.id , sc.course_name');
        $this->core_read->from('slii_courses as sc');
        $query = $this->core_read->get();
        if(!$query->num_rows()){
            return false;
        }
        return $query->result_array();
    }

    public function assign_for_courses($course_data){
        $this->db->insert('slii_member_course_pivot',$course_data);
        $response['type'] = 'success';
        $response['message'] = 'Record Added Successfully';
        return $response;
    }

    public function get_my_courses(){
        $login_data = $this->session->userdata('login_data');
        $this->core_read->select('sc.course_name, smc.assign_subjects as subjects,sc.compulsory_subjects');
        $this->core_read->from('slii_member_course_pivot as smc');
        $this->core_read->join('slii_courses sc', 'sc.id = smc.fk_course_id' , 'left');
        $this->core_read->where('smc.fk_member_id', $login_data['id']);
        $query = $this->core_read->get();
        if(!$query->num_rows()){
            return false;
        }
        $my_course_data = $query->result_array();

        $course_set = array();
        foreach ($my_course_data as $course){
            $subjects = explode(',',$course['subjects']);
            $compulsory_subjects = explode(',',$course['compulsory_subjects']);
            $subjects_data = array();
            foreach ($subjects as $sub_id){
                $this->core_read->select('ss.subject_name,ss.id');
                $this->core_read->from('slii_subjects as ss');
                $this->core_read->where('ss.id',$sub_id);
                $query = $this->core_read->get();
                if(!$query->num_rows()){
                    return false;
                }else{
                    $sub_name = $query->row_array()['subject_name'];
                    $sub_id = $query->row_array()['id'];
                    $sub = array('name' => $sub_name , 'id' => $sub_id);
                    array_push($subjects_data , $sub);
                }
            }
            $course['subjects'] = $subjects_data;
            $course['compulsory_subjects'] = $compulsory_subjects;
            array_push($course_set , $course);
        }
        return $course_set;
    }

    public function get_subject_name_from_id($id){

    }

    public function get_subjects_for_course($where_data){
        $this->core_read->select('sc.subjects,sc.compulsory_subjects');
        $this->core_read->from('slii_courses as sc');
        $this->core_read->where($where_data);
        $query = $this->core_read->get();
        if(!$query->num_rows()){
            return false;
        }
        else{
            $res = $query->result_array();
            $subjects = explode(',' , $res[0]['subjects']);
            $compulsory_subjects = explode(',' , $res[0]['compulsory_subjects']);
            $ids = array();
            foreach ($subjects as $id)
            {
                $ids[] = $id;
            }
            $this->core_read->select("
            ss.id,
            ss.subject_name,
            ss.subject_code
            ");
            $this->core_read->from('slii_subjects as ss');
            $this->core_read->where_in('ss.id' , $ids);
            $query = $this->core_read->get();
            if(!$query->num_rows()){
                return false;
            }else{
                $subject = $query->result_array();
                $subject_set = array();
                foreach ($subject as $sub){
                    if( in_array($sub['id'],$compulsory_subjects) ){
                        $sub['is_compulsory'] = 1;
                    }else{
                        $sub['is_compulsory'] = 0;
                    }
                    array_push($subject_set , $sub);
                }
            }
            return $subject_set;
        }
    }

}
