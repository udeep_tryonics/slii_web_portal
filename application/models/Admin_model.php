<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends TRY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function change_sliders(){
        $this->core_read->select('*');
        $this->core_read->from('slii_slider_images');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_all_services(){
        $this->core_read->select('*');
        $this->core_read->from('slii_services');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_activities(){
        $this->core_read->select('*');
        $this->core_read->from('slii_activities');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function change_portfolio(){
        $this->core_read->select('*');
        $this->core_read->from('slii_gallery_images');
        $this->core_read->where('status' , 1);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function photo_gallery(){
        $this->core_read->select('sgi.id,sgi.file_name,sgi.title,sgi.status,sgi.album_id,sal.album_name');
        $this->core_read->from('slii_gallery_images sgi');
        $this->core_read->join('slii_albums sal' , 'sal.id = sgi.album_id' , 'left');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function recent_works($where){
        $this->core_read->select('*');
        $this->core_read->from('slii_recent_works');
        $this->core_read->where($where);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_sliders(){
        $this->core_read->select('*');
        $this->core_read->from('slii_slider_images');
        $this->core_read->where('status' , 1);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function remove_slider_img($img_id){
        $this->core_write->where('id' , $img_id);
        $this->core_write->delete('slii_slider_images');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function delete_service($where){
        $this->core_write->where($where);
        $this->core_write->delete('slii_services');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function remove_gallery_img($img_id){
        $this->core_write->where('id' , $img_id);
        $this->core_write->delete('slii_gallery_images');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function remove_recent_works_img($img_id){
        $this->core_write->where('id' , $img_id);
        $this->core_write->delete('slii_recent_works');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function add_slider_image($array_images){
        $res = $this->core_write->insert('slii_slider_images' , $array_images);
        if($res){
            $response = array('type' => 'success', 'message' => 'Successfuly add your slider.');
            return $response;
        }else{
            $response = array('type' => 'error', 'message' => 'Error occurred.');
            return $response;
        }
    }

    public function add_recent_works_image($array_images){
        $res = $this->core_write->insert('slii_recent_works' , $array_images);
        if($res){
            $response = array('type' => 'success', 'message' => 'Successfuly add your slider.');
            return $response;
        }else{
            $response = array('type' => 'error', 'message' => 'Error occurred.');
            return $response;
        }
    }

    public function add_gallery_image($array_images){
        $res = $this->core_write->insert('slii_gallery_images' , $array_images);
        if($res){
            $response = array('type' => 'success', 'message' => 'Successfuly add your gallery.');
            return $response;
        }else{
            $response = array('type' => 'error', 'message' => 'Error occurred.');
            return $response;
        }
    }

    public function add_albums($array_images){
        $res = $this->core_write->insert('slii_albums' , $array_images);
        if($res){
            $response = array('type' => 'success', 'message' => 'Successfuly add your album.');
            return $response;
        }else{
            $response = array('type' => 'error', 'message' => 'Error occurred.');
            return $response;
        }
    }

    public function add_subject($subject){
        $res = $this->core_write->insert('slii_subjects' , $subject);
        if($res){
            $response = array('type' => 'success', 'message' => 'Successfuly saved.');
            return $response;
        }else{
            $response = array('type' => 'error', 'message' => 'Error occurred.');
            return $response;
        }
    }

    public function add_feed($news_data){
        if( $news_data['id'] ){
            $this->core_write->where('id',$news_data['id']);
            $this->core_write->update('slii_news_feeds',$news_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
            return $response;
        }else{
            $this->core_write->insert('slii_news_feeds',$news_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Added';
            return $response;
        }
    }

    public function add_activities($activity_data){
        if( $activity_data['id'] ){
            $this->core_write->where('id',$activity_data['id']);
            $this->core_write->update('slii_activities',$activity_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
            return $response;
        }else{
            $this->core_write->insert('slii_activities',$activity_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Added';
            return $response;
        }
    }

    public function set_active_album($where_array , $data_array){
        $this->core_write->where($where_array);
        $this->core_write->update('slii_albums', $data_array);
        $response['type'] = 'success';
        $response['message'] = 'Successfully Updated';
        return $response;
    }

    public function get_all_news_feeds(){
        $this->core_read->select('*');
        $this->core_read->from('slii_news_feeds');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_albums(){
        $this->core_read->select('id,album_name,album_image');
        $this->core_read->from('slii_albums');
        $this->core_read->where('status' , 1);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_all_subjects(){
        $this->core_read->select('*');
        $this->core_read->from('slii_subjects');
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function delete_news_feed($where_array){
        $this->core_write->where($where_array);
        $this->core_write->delete('slii_news_feeds');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function delete_album($where_array){
        $this->core_write->where($where_array);
        $this->core_write->delete('slii_albums');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function delete_activities($where_array){
        $this->core_write->where($where_array);
        $this->core_write->delete('slii_activities');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

    public function activate_user($where_array,$update_data){
        $this->core_write->where($where_array);
        $this->core_write->update('slii_users',$update_data);
        $response['type'] = 'success';
        $response['message'] = 'Successfully Updated';
        return $response;
    }

    public function add_course($course_data,$course_id){
        if($course_id){
            $this->core_write->where('id',$course_id);
            $this->core_write->update('slii_courses',$course_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
            return $response;
        }else{
            $this->core_write->insert('slii_courses',$course_data);
            $response['type'] = 'success';
            $response['message'] = 'Successfully Added';
            return $response;
        }
    }

    public function get_payments_for_course($where_array){
        $this->core_read->select('*');
        $this->core_read->from('slii_course_payment');
        $this->core_read->where($where_array);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function get_album_images($where_array){
        $this->core_read->select('sgi.title ,sgi.file_name,sgi.id');
        $this->core_read->from('slii_albums sa');
        $this->core_read->join('slii_gallery_images sgi' , 'sgi.album_id = sa.id' , 'left');
        $this->core_read->where($where_array);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function add_contact_details($contact_details){
        $res = $this->core_write->insert('slii_contact_informations',$contact_details);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Sent your message';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function update_gallery_img($update_data , $where){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_gallery_images',$update_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function update_recent_works_img($update_data , $where){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_recent_works',$update_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function set_active_image($update_data , $where){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_gallery_images',$update_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function set_active_recent_works($update_data , $where){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_recent_works',$update_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function set_active_slider($update_data , $where){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_slider_images',$update_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function get_all_videos($where){
        $this->core_read->select('*');
        $this->core_read->from('slii_videos');
        $this->core_read->where($where);
        $data = $this->core_read->get()->result_array();
        return $data;
    }

    public function add_video($video_details){
        $res = $this->core_write->insert('slii_videos',$video_details);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function add_services($service_data){
        $res = $this->core_write->insert('slii_services',$service_data);
        if($res){
            $response['type'] = 'success';
            $response['message'] = 'Successfully Updated';
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function set_active_video($update_data , $where , $where_inactive , $update_data_inactive){
        $this->core_write->where($where);
        $res = $this->core_write->update('slii_videos',$update_data);
        if($res){
            $this->core_write->where($where_inactive);
            $res2 = $this->core_write->update('slii_videos',$update_data_inactive);
            if($res) {
                $response['type'] = 'success';
                $response['message'] = 'Successfully Updated';
            }else{
                $response['type'] = 'error';
                $response['message'] = 'Error Ocuured';
            }
        }else{
            $response['type'] = 'error';
            $response['message'] = 'Error Ocuured';
        }
        return $response;
    }

    public function remove_video($where_array){
        $this->core_write->where($where_array);
        $this->core_write->delete('slii_videos');
        $response['type'] = 'success';
        $response['message'] = 'Successfully deleted';
        return $response;
    }

}
