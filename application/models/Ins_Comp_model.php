<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ins_Comp_model extends TRY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function member_registration_submit($member_data,$register_data,$slii_member_edit_id,$experience_list){

        if( $slii_member_edit_id ){
            $this->db->where(array('id'=> $slii_member_edit_id));
            $this->db->update('slii_member',$member_data);

            $this->db->where(array('member_id'=> $slii_member_edit_id));
            $this->db->delete('slii_member_insurance_experience');

            foreach ($experience_list as $exp){
                $exp['member_id'] = $slii_member_edit_id;
                $this->db->insert('slii_member_insurance_experience',$exp);
            }

            $response['type'] = 'success';
            $response['message'] = 'Record Updated Successfully';
            return $response;

        }else{
            $this->db->insert('slii_users',$register_data);
            $last_insert_user_id = $this->db->insert_id();
            $register_user_role = array(
                'fk_user_id' => $last_insert_user_id,
                'fk_group_id' => 5,
            );
            $this->db->insert('slii_user_group_pivot',$register_user_role);
            $member_data['fk_slii_user_id'] = $last_insert_user_id;
            $this->db->insert('slii_member',$member_data);

            $member_last_id = $this->db->insert_id();

            foreach ($experience_list as $exp){
                $exp['member_id'] = $member_last_id;
                $this->db->insert('slii_member_insurance_experience',$exp);
            }

            $response['type'] = 'success';
            $response['message'] = 'Record Added Successfully';
            return $response;
        }
    }

    public function get_insurance_member_details($member_user_id){
        $this->core_read->select('sm.id as slii_member_edit_id,sm.fullname,sm.surname,sm.dob,sm.nic,sm.cmp_name_adr,sm.cmp_phone,sm.present_occupation,sm.cmp_email,sm.home_adr,sm.home_phone,sm.home_mobile,sm.home_email,sm.current_industry_nature,sm.insurance_type,sm.ins_areas,sm.edu_qualifications
    ,sm.pref_mail_adr,su.payment_type');
        $this->core_read->from('slii_member as sm');
        $this->core_read->join('slii_users su', 'su.id = sm.fk_slii_user_id' , 'left');
        $this->core_read->where(array('sm.id'=> $member_user_id));
        $query = $this->core_read->get();
        if($query->num_rows() != 1){
            return false;
        }
        return $query->row_array();
    }

    public function get_insurance_member_exp_details($member_user_id){
        if( !$member_user_id ){
            $member_user_id = -1;
        }
        $this->core_read->select('*');
        $this->core_read->from('slii_member_insurance_experience as sm_ins_exp');
        $this->core_read->where(array('sm_ins_exp.member_id'=> $member_user_id));
        $query = $this->core_read->get();
        if(!$query->num_rows()){
            return false;
        }
        return $query->result_array();
    }

    public function get_insurance_company_all(){
        $this->core_read->select('*');
        $this->core_read->from('slii_company as sc');
        $query = $this->core_read->get();
        if(!$query->num_rows()){
            return false;
        }
        return $query->result_array();
    }
}
