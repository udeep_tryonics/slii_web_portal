<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stat_model extends TRY_Model
{

    function __construct()
    {
        parent::__construct();
    }

    public function get_call_count(array $filters=array()){
        $sql = "SELECT COUNT(tc.id) as count,ts.id FROM try_agent_comments tc INNER JOIN (SELECT agent_number_id, MAX(`created_at`) AS created_at FROM try_agent_comments GROUP BY agent_number_id) tmp ON tc.agent_number_id = tmp.agent_number_id AND tc.created_at = tmp.created_at JOIN try_call_status ts ON tc.call_status = ts.id JOIN try_agent_numbers tn ON tn.id=tc.agent_number_id ";
        $values = array();
        if(!empty($filters)){
            $values = array_values($filters);
            $sql .= $this->make_filters($filters);
        }
        $sql .=" GROUP BY tc.call_status";
        $query = $this->db->query($sql,$values);
        return $query->result_array();
    }

    public function get_pending_vs_other_count(array $filters = array()){
        $sql = 'SELECT COUNT(*) as count, IF(tc.agent_number_id IS NULL ,"Pending","Completed") as status from try_agent_numbers tn LEFT JOIN (select DISTINCT agent_number_id from try_agent_comments) tc ON tn.id = tc.agent_number_id';
        $values = array();
        if(!empty($filters)){
            $values = array_values($filters);
            $sql .= $this->make_filters($filters);
        }
        $sql .=" GROUP BY status";
        $query = $this->db->query($sql,$values);
        return $query->result_array();
    }
    private function make_filters(array $filters){
        $keys = array_keys($filters);
        $where = ' WHERE ';
        foreach($keys as $cond){
            $where .= " $cond = ? AND";
        }
        return $where  = substr($where,0,-3);
    }
}
