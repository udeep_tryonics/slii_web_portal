<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Title           Auth Model
 *
 * @package        Tryonics
 * Location        application/models/Auth_model.php
 *
 * @author         ruwan - <ruwanpthmalal@gmail.com>
 * @copyright      Tryonics
 *
 * created on      27/09/2017, 12:21 PM by ruwan
 *
 * Description     Interact with database to handle authentication related data.
 *
 * */
class Auth_model extends TRY_Model{

    /**
     * __construct
     *
     * @param none
     * @access public
     * @author ruwan - <ruwanpthmalal@gmail.com>
     * */
    function __construct(){
        parent::__construct();
    }

    /**
     * Get user by username and password from database
     * @access public
     * @param String $username
     * @return mixed|false $array if record found return array else return false
     * @author ruwan - <ruwanpthmalal@gmail.com>
     * */
    public function get_user_by_username_password($username,$password){
        $this->core_read->select('u.id,u.username,u.company,sc.cmp_description as companyname');
        $this->core_read->from('slii_users as u');
        $this->core_read->join('slii_company sc', 'sc.cmp_id = u.company' , 'left');
        $this->core_read->where(array('u.password'=>$this->hash_password($password),'u.username'=>$username));
        $query = $this->core_read->get();
        if($query->num_rows() != 1){
            return false;
        }
        return $query->row_array();
    }

    /**
     * Get the hash password with md5
     * @access public
     * @param String $password
     * @return String sha1 hash
     * @author ruwan - <ruwanpthmalal@gmail.com>
     * */
    public function hash_password($password){
        return md5($password);
    }

    /**
     * update user data based on condition.
     * @access public
     * @param mixed $data
     * @param mixed $where
     * @return int|bool
     * @author ruwan - <ruwanpthmalal@gmail.com>
     * */
    public function update($data,$where=array()){
        if(!empty($where)){
            $this->core_write->where($where);
            $status = $this->core_write->update($this->config->item('users', 'masterdb_config') . ' as u',$data);
            if($status){
                return $this->core_write->affected_rows();
            }
            return false;
        }
        return false;
    }

    /**
     * Get User assigned groups.
     * @access public
     * @param int $user_id
     * @return mixed $assigned_groups
     * @author ruwan - <ruwanpthmalal@gmail.com>
     * */
    public function get_user_assigned_group($user_id){
        $this->core_read->select('sg.groupname');
        $this->core_read->from('slii_user_group_pivot as sug');
        $this->core_read->join('slii_groups sg', 'sg.id = sug.fk_group_id' , 'left');
        $this->core_read->join('slii_users su', 'su.id = sug.fk_user_id' , 'left');
        $this->core_read->where('su.id',$user_id);
        $group_set = $this->core_read->get()->result_array();
        $assign_group = array();
        foreach ($group_set as $gr){
            array_push($assign_group , strtolower($gr['groupname']) );
        }
        return $assign_group;
    }

    public function reset_password_user($new_pw,$user_name)
    {
        $this->core_write->where('us.username',$user_name);
        if($this->core_write->update('slii_users us',array('password' => $this->hash_password($new_pw)))){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}