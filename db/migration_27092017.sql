CREATE TABLE `try_agent_numbers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` varchar(255) DEFAULT NULL,
  `agent_numbers` varchar(255) DEFAULT NULL,
  `contract_no` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `age` varchar(255) DEFAULT NULL,
  `sales_person` varchar(255) DEFAULT NULL,
  `group_code` int(11) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;

CREATE TABLE `try_agent_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_number_id` varchar(255) DEFAULT NULL,
  `agent_comment` varchar(255) DEFAULT NULL,
  `call_status` varchar(10) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;

CREATE TABLE `try_call_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `try_user_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(1) DEFAULT '0',
  `is_read` tinyint(1) DEFAULT '0',
  `notification_type` int(11) DEFAULT NULL,
  `notification_message` varchar(255) DEFAULT NULL,
  `agent_id` int(11) DEFAULT NULL,
  `active_time_from` datetime DEFAULT NULL,
  `active_time_to` datetime DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `try_schedule_calls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) DEFAULT NULL,
  `schedule_time_from` datetime DEFAULT NULL,
  `agent_numbers` int(11) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT '0',
  `schedule_time_to` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `agent_number_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO try_call_status (id, description, active) VALUES (1, 'Call Answered', 1);
INSERT INTO try_call_status (id, description, active) VALUES (2, 'Call Rejected', 1);
INSERT INTO try_call_status (id, description, active) VALUES (3, 'Line Busy', 1);
INSERT INTO try_call_status (id, description, active) VALUES (4, 'Call Scheduled', 1);