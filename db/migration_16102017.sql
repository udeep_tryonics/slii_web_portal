CREATE TABLE `try_breaks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `break_name` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;

CREATE TABLE `try_assign_breaks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL,
  `break_id` int(11) NOT NULL,
  `start_date_time` datetime NOT NULL,
 
  `end_date_time` datetime NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;

CREATE TABLE `try_agent_break_tracker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_id` int(11) NOT NULL,
  `break_id` int(11) NOT NULL,
  `start_date_time` datetime NOT NULL, 
  `end_date_time` datetime DEFAULT NULL,
  `estimated_start_date_time` datetime DEFAULT NULL,
  `estimated_end_date_time` datetime DEFAULT NULL,
  `time_difference` varchar(50) DEFAULT NULL, 
  `difference_status` varchar(1) DEFAULT '1' COMMENT "e - exceed, r - recede, 1 - same as estimated" ,
  `record_status` varchar(1) DEFAULT 's' COMMENT "s - system, m - manual" ,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL, 
  `updated_by` int(11) DEFAULT NULL,   
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1;